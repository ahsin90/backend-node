let Users = require('../models/users.model');

module.exports = function check_token(token, calback)
{
    Users.findOne({'sessions.token' : token}, function (err, result){
        if (err) throw err;
        if (result == null ){
            calback(false);
        }
        else {
            var ret = new Date() <= result.sessions[0].expired_date;
            calback(ret);
        }
    });
};