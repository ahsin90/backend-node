const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Batas_provinsi = new Schema({
    _id: ObjectId,
    provinsi_id: Number,
    nama: String,
    nomor_izin: String,
    tanggal_berita_acara: Date,
    file_sertifikat: String,
    nomor_pilar: String,
    warna_palette: String,
    file_geojson: String,
    luas: Number,
    status: String,
    geolocation: {
        type: { type: String },
        coordinates: []
    }
},{collection : "batas_provinsi"});

module.exports = mongoose.model('Batas_provinsi', Batas_provinsi);