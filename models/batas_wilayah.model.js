const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Batas_wilayah = new Schema({
    _id: ObjectId,
    kode: String,
    level: String,
    kabupaten_id: Number,
    provinsi_id: Number,
    kecamatan_id: Number,
    desa_id: Number,
    foto_depan: String,
    foto_belakang: String,
    foto_kanan: String,
    foto_kiri: String,
    foto_atas: String,
    file_geojson: String,
    status: String,
    geolocation: {
        type: { type: String },
        coordinates: []
    }

    },{collection : "batas_wilayah"});


module.exports = mongoose.model('Batas_wilayah', Batas_wilayah);
