const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Izin_usaha_pertambangan = new Schema({
    _id: ObjectId,
    kode: String,
    nama: String,
    kabupaten_id: Number,
    provinsi_id: Number,
    kecamatan_id: Number,
    desa_id: Number,
    jenis_galian: String,
    nomor_sk: String,
    tanggal_sk: Date,
    tanggal_berakhir: Date,
    file_sk: String,
    file_geojson: String,
    luas: Number,
    status: String,
    created_at: Date,
    updated_at: Date,
    geolocation: {
        type: { type: String },
        coordinates: []
    }

    },{collection : "izin_usaha_pertambangan"});


module.exports = mongoose.model('Izin_usaha_pertambangan', Izin_usaha_pertambangan);
