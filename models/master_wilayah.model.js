const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Wilayah = new Schema({
    _id: ObjectId,
    id: Number,
    kode: String,
    nama: String,
    gampong: Number,
    kecamatan: Number,
    kabupaten: Number,
    provinsi: Number,
    domain: String,
    alias: String,
    folder: String,
    aktif: Number,
    tingkat: Number

    },{collection : "wilayah"});


module.exports = mongoose.model('wilayah', Wilayah);
