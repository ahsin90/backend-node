const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Modules = new Schema({
    _id: ObjectId,
    module_name: String,
    module_url: String,
    module_icon: String,
    is_active: Boolean,
    submodules: [
        {
            submodule_id: ObjectId,
            submodule_name: String,
            submodule_url: String,
            submodule_icon: String
        }
    ]
}, {collection : "modules"});

module.exports = mongoose.model('Modules', Modules);