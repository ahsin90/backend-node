const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let OPD = new Schema({
    nama_opd: String,
	akronim: String,
	website: String,
	alamat: String,
	telepon: String,
	level: String,
	kabupaten_id: Number,
	kecamatan_id: Number,
	location: {
        type: { type: String },
        coordinates: []
    }
    },{collection : "opd"});

OPD.index({ location: "2dsphere" });

module.exports = mongoose.model('OPD', OPD);
