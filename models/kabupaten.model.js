const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Kabupaten = new Schema({
    kabupaten_id: Number,
    provinsi_id: Number,
    kabupaten: String
    },{collection : "kabupaten"});


module.exports = mongoose.model('Kabupaten', Kabupaten);
