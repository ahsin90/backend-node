const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Bangunan = new Schema({
    _id: ObjectId,
    kode: String,
    kabupaten_id: Number,
    provinsi_id: Number,
    kecamatan_id: Number,
    desa_id: Number,
    nomor_sertifikat: String,
    tanggal_sertifikat: Date,
    luas: Number,
    nama: String,
    nama_tertera: String,
    nomor_izin: String,
    tanggal_izin: Date,
    peruntukan: String,
    file_sertifikat: String,
    foto_depan: String,
    foto_belakang: String,
    foto_kanan: String,
    foto_kiri: String,
    foto_atas: String,
    file_geojson: String,
    status: String,
    geolocation: {
        type: { type: String },
        coordinates: []
    }

    },{collection : "tanah_bangunan"});


module.exports = mongoose.model('Bangunan', Bangunan);
