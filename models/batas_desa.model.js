const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Batas_desa = new Schema({
    _id: ObjectId,
    provinsi_id: Number,
    kabupaten_id: Number,
    kecamatan_id: Number,
    desa_id: Number,
    nomor_izin: String,
    tanggal_berita_acara: Date,
    file_sertifikat: String,
    nomor_pilar: String,
    warna_palette: String,
    file_geojson: String,
    status: String,
    geolocation: {
        type: { type: String },
        coordinates: []
    }
},{collection : "batas_desa"});

module.exports = mongoose.model('Batas_desa', Batas_desa);
