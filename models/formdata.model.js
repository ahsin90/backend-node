const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let FormData = new Schema({
    _id: ObjectId,
    submodule_id : ObjectId,
}, {collection : "form_data", strict: false});

module.exports = mongoose.model('FormData', FormData);