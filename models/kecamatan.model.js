const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Kecamatan = new Schema({
    kabupaten_id: Number,
    kecamatan_id: Number,
    kecamatan: String
    },{collection : "kecamatan"});


module.exports = mongoose.model('Kecamatan', Kecamatan);
