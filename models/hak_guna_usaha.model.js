const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Hak_guna_usaha = new Schema({
    _id: ObjectId,
    kode: String,
    nama: String,
    kabupaten_id: Number,
    provinsi_id: Number,
    kecamatan_id: Number,
    desa_id: Number,
    fungsi: String,
    nomor_sk: String,
    tanggal_sk: Date,
    tanggal_berakhir: Date,
    file_sk: String,
    file_geojson: String,
    luas: Number,
    peruntukan: String,
    status: String,
    created_at: Date,
    updated_at: Date,
    geolocation: {
        type: { type: String },
        coordinates: []
    }

    },{collection : "hak_guna_usaha"});


module.exports = mongoose.model('Hak_guna_usaha', Hak_guna_usaha);
