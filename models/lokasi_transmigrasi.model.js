const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Lokasi_transmigrasi = new Schema({
    _id: ObjectId,
    kode: String,
    nama: String,
    kabupaten_id: Number,
    provinsi_id: Number,
    kecamatan_id: Number,
    desa_id: Number,
    nomor_sk: String,
    tanggal_sk: Date,
    tanggal_berakhir: Date,
    file_sk: String,
    file_geojson: String,
    luas: Number,
    file_peserta: String,
    status: String,
    created_at: Date,
    updated_at: Date,
    geolocation: {
        type: { type: String },
        coordinates: []
    }

    },{collection : "lokasi_transmigrasi"});


module.exports = mongoose.model('Lokasi_transmigrasi', Lokasi_transmigrasi);
