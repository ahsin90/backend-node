const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Logs = new Schema({
    _id: ObjectId,
    message: String,
    user_id: ObjectId,
    created_at: Date,
    ip_address : String
}, {collection : "app_logs"});

module.exports = mongoose.model('Logs', Logs);