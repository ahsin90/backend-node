const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Groups = new Schema({
    _id: ObjectId,
    group_name: String,
    description: String,
    modules: []
}, {collection : "groups"});

module.exports = mongoose.model('Groups', Groups);