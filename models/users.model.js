const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Users = new Schema({
    username: String,
    nik: String,
    nama: String,
    tempat_lahir: String,
    email: String,
    tanggal_lahir: String,
    alamat: String,
    jabatan: String,
    phone: String,
    sk: String,
    wilayah_tugas: Number,
    password: String,
    is_active: Boolean,
    last_login: Date,
    opd_id: ObjectId,
    group_id: ObjectId,
    is_verifikator: Boolean,
    sessions: [
        {
            token: String,
            expired_date: Date
        }
    ],
    created_at: Date,
    updated_at: Date
    }, {collection : "users"});

module.exports = mongoose.model('Users', Users);