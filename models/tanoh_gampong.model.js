const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Tanoh_gampong = new Schema({
    _id: ObjectId,
    kode: String,
    nama_subjek: String,
    nik: String,
    nomor_telepon: String,
    tanda_bukti_hak: String,
    status_tanah: String,
    lokasi: String,
    jenis_tanah: String,
    patok_tanda_batas: String,
    no_pbb: String,
    status_pbb: String,
    batas_utara: String,
    batas_barat: String,
    batas_selatan: String,
    batas_timur: String,
    luas: Number,
    kelas_tanah: String,
    papan_nama: String,
    mutasi: String,
    riwayat_penguasaan_objek: String,
    kronologis_masalah: String,
    kronologis_penyelesaian: String,
    file_sertifikat: String,
    foto_depan: String,
    foto_belakang: String,
    foto_kanan: String,
    foto_kiri: String,
    foto_atas: String,
    file_geojson: String,
    status: String,
    created_at: Date,
    updated_at: Date,
    geolocation: {
        type: { type: String },
        coordinates: []
    }
},{collection : "tanoh_gampong"});

module.exports = mongoose.model('Tanoh_gampong', Tanoh_gampong);
