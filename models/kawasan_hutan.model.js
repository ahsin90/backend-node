const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Kawasan_hutan = new Schema({
    _id: ObjectId,
    kode: String,
    nama: String,
    kabupaten_id: Number,
    provinsi_id: Number,
    kecamatan_id: Number,
    fungsi: String,
    nomor_sk: String,
    tanggal_sk: Date,
    file_sk: String,
    file_geojson: String,
    luas: Number,
    status: String,
    created_at: Date,
    updated_at: Date,
    geolocation: {
        type: { type: String },
        coordinates: []
    }

    },{collection : "kawasan_hutan"});


module.exports = mongoose.model('Kawasan_hutan', Kawasan_hutan);
