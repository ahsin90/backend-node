const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Desa = new Schema({
    desa_id: Number,
    kecamatan_id: Number,
    desa: String
    },{collection : "desa"});


module.exports = mongoose.model('Desa', Desa);
