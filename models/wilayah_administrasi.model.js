const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Wilayah_administrasi = new Schema({
    _id: ObjectId,
    kode: String,
    kabupaten_id: Number,
    provinsi_id: Number,
    kecamatan_id: Number,
    desa_id: Number,
    uu_penetapan: String,
    tanggal_penetapan: Date,
    file_sertifikat: String,
    foto_depan: String,
    foto_belakang: String,
    foto_kanan: String,
    foto_kiri: String,
    foto_atas: String,
    file_geojson: String,
    status: String,
    created_at: Date,
    geolocation: {
        type: { type: String },
        coordinates: []
    }

    },{collection : "wilayah_administrasi"});


module.exports = mongoose.model('Wilayah_administrasi', Wilayah_administrasi);
