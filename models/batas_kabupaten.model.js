const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Batas_Kabupaten = new Schema({
    _id: ObjectId,
    provinsi_id: Number,
    nama: String,
    kabupaten_id: Number,
    nomor_izin: String,
    tanggal_berita_acara: Date,
    file_sertifikat: String,
    nomor_pilar: String,
    luas: Number,
    warna_palette: String,
    file_geojson: String,
    status: String,
    geolocation: {
        type: { type: String },
        coordinates: []
    }
},{collection : "batas_kabupaten"});


module.exports = mongoose.model('Batas_kabupaten', Batas_Kabupaten);
