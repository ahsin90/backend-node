const mongoose = require('mongoose');
const Schema = mongoose.Schema;
var ObjectId = require('mongodb').ObjectID;

let Provinsi = new Schema({
    provinsi_id: Number,
    provinsi: String,
    geolocation: {
        type: { type: String },
        coordinates: []
    }
    },{collection : "provinsi"});

module.exports = mongoose.model('Provinsi', Provinsi);
