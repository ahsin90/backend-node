var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var mongo = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

let Users = require('../models/users.model');
let Otentikasi = require('../helpers/otentikasi');
let Provinsi = require('../models/provinsi.model');
let Kabupaten = require('../models/kabupaten.model');
let Kecamatan = require('../models/kecamatan.model');
let Desa = require('../models/desa.model');

router.get('/provinsi/list/', async function(req, res, next) {
    const token         = req.header('api_token');
    let keyword         = req.query.search;
    let limit           = parseInt( req.query.limit );

    let offset = 0;
    if( keyword !== null ){
        offset = parseInt( req.query.offset );
    }

    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Provinsi.count();

    let search = new RegExp(keyword, 'i');
    Provinsi.find({ provinsi: search} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            recordsTotal: total,
            recordsFiltered: limit,
            data: data
        } );
    }).limit(limit).skip(offset).sort({provinsi_id:1});

});

router.post('/provinsi/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
            Provinsi.findOne({provinsi_id : req.body.provinsi_id}, function (err, doc){

                if (doc != null){
                    res.json({status : 'failed', data : 'Kode provinsi sudah ada'});
                }
                else{
                    let prov = new Provinsi(req.body);
                    prov.save().then(prov =>{
                        res.json({status : 'success', data : true});
                    }).catch(err =>{
                        throw err;
                    });
                }
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/provinsi/get/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Provinsi.findOne({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
         else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/provinsi/edit/:id', function (req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            
            Provinsi.findOneAndUpdate({_id : new ObjectId(id)}, {$set:req.body}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.delete('/provinsi/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Provinsi.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/provinsi/get/', function(req, res, next) {
    Provinsi.find({} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
                        status: "success",
            data: data
        } );
    }).sort({provinsi_id:1});

});

router.get('/kabupaten/list/', async function(req, res, next) {
    const token         = req.header('api_token');
    let keyword          = req.query.search;
    let limit           = parseInt( req.query.limit );
    let offset           = parseInt( req.query.offset );
    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Kabupaten.count({provinsi_id:11});

    let search = new RegExp(keyword, 'i');
    Kabupaten.find({ kabupaten: search, provinsi_id: 11} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            recordsTotal: total,
            recordsFiltered: limit,
            data: data
        } );
    }).limit(limit).skip(offset).sort({kabupaten:1});

});

router.get('/kabupaten/list_public/:id', async function(req, res, next) {
    const prov_id     = req.params.id;
    Kabupaten.find({ provinsi_id: prov_id} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {status : 'success', data: data} );
    }).sort({kabupaten_id:1});

});

router.post('/kabupaten/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
            Kabupaten.findOne({kabupaten_id : req.body.kabupaten_id}, function (err, doc){
                console.log( doc )
                if (doc != null){
                    res.json({status : 'failed', data : 'Kode kabupaten sudah tersedia'});
                }
                else{
                    let prov = new Kabupaten(req.body);
                    prov.save().then(prov =>{
                        res.json({status : 'success', data : true});
                    }).catch(err =>{
                        throw err;
                    });
                }
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/kabupaten/get/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Kabupaten.findOne({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
         else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/kabupaten/edit/:id', function (req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
                    console.log( req.body );
            Kabupaten.findOneAndUpdate({_id : new ObjectId(id)}, {$set:req.body}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.delete('/kabupaten/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Kabupaten.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/kabupaten/get_by_provinsi/:provinsi_id', function(req, res, next) {
    let id = req.params.provinsi_id;
    Kabupaten.find({provinsi_id: id} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
                        status: "success",
            data: data
        } );
    }).sort({kabupaten:1});

});


router.get('/kecamatan/list/', async function(req, res, next) {
    const token         = req.header('api_token');
    let keyword         = req.query.search;
    let limit           = parseInt( req.query.limit );
    let offset          = parseInt( req.query.offset );
    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Kecamatan.count();

    let search = new RegExp(keyword, 'i');
    let kecamatan = await Kecamatan.aggregate([
           {
             $lookup:
               {
                 from: "kabupaten",
                 localField: "kabupaten_id",
                 foreignField: "kabupaten_id",
                 as: "kabupaten"
               }
          }
        ]).match({kecamatan: search}).limit(limit).skip(offset).sort({kecamatan_id:1});

    res.json( {
        recordsTotal: total,
        recordsFiltered: limit,
        data: kecamatan
    } );
});

router.get('/kecamatan/list_public_by_kabupaten_id/:id', async function(req, res, next) {
    const token         = req.header('api_token');
    let ids = req.params.id.split(",");

    Kecamatan.find({kabupaten_id: ids} , function (err, result) {
        if (err) throw err;

        res.json( {status: 'success', data: result});
    }).sort({kecamatan_id:1});
});

router.post('/kecamatan/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
            Kecamatan.findOne({kecamatan_id : req.body.kecamatan_id}, function (err, doc){

                if (doc != null){
                    res.json({status : 'failed', data : 'Kode kecamatan sudah ada'});
                }
                else{
                    let prov = new Kecamatan(req.body);
                    prov.save().then(prov =>{
                        res.json({status : 'success', data : true});
                    }).catch(err =>{
                        throw err;
                    });
                }
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/kecamatan/get/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Kecamatan.findOne({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;

                console.log( result );

                res.json({status: 'success', data: result});
            });
        }
         else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/kecamatan/get_by_id/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Kecamatan.findOne({kecamatan_id: id}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/kecamatan/edit/:id', function (req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Kecamatan.findOneAndUpdate({_id : new ObjectId(id)}, {$set:req.body}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.delete('/kecamatan/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Kecamatan.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/kecamatan/get_by_kabupaten/:kabupaten_id', function(req, res, next) {
    let id = req.params.kabupaten_id;
    
    Kecamatan.find({kabupaten_id: id} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
                        status: "success",
            data: data
        } );
    }).sort({kecamatan:1});

});

router.get('/desa/get_by_kecamatan/:kecamatan_id', function(req, res, next) {
    let id = req.params.kecamatan_id;
    
    Desa.find({kecamatan_id: id} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
                        status: "success",
            data: data
        } );
    }).sort({desa:1});

});

router.get('/desa/list/', async function(req, res, next) {
    const token         = req.header('api_token');
    let keyword         = req.query.search;
    let limit           = parseInt( req.query.limit );
    let offset          = parseInt( req.query.offset );
    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Kecamatan.count();

    let search = new RegExp(keyword, 'i');
    let desa = await Desa.aggregate([
           {
             $lookup:
               {
                 from: "kecamatan",
                 localField: "kecamatan_id",
                 foreignField: "kecamatan_id",
                 as: "kecamatan"
               }
          }
        ]).match({desa: search}).limit(limit).skip(offset).sort({desa_id:1});

    res.json( {
        recordsTotal: total,
        recordsFiltered: limit,
        data: desa
    } );
});

module.exports = router;
