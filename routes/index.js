var express = require('express');
var router = express.Router();
var crypto = require('crypto');
let Users = require('../models/users.model');
let Otentikasi = require('../helpers/otentikasi');
let Recaptcha = require('express-recaptcha').RecaptchaV3;
let recaptcha = new Recaptcha('6LcnTdcUAAAAAIYekihq-UkrZoo4gHiU-PxokqsG', '6LcnTdcUAAAAAAseJCuVIgc1jSWpIY8uEmYfJyOD');


/* GET home page. */
// router.get('/', function(req, res, next) {
//   res.render('index', { title: 'SIM Tanah Home Page' });
// });

router.get('/', function(req, res, next) {
    res.json({ status: "Index page" })
});

router.post('/login', function(req, res, next) {
  req.body.password = crypto.createHash('sha256').update(req.body.password).digest('hex');

  Users.findOne({username:req.body.username, password:req.body.password}, function (err, result) {
    if (err) throw err;
    if (result == null){
      res.json({status: 'fail', data: 'Email atau password yang anda masukkan salah.'});
    }
    else{
      result.password = null;
      res.json({status: 'success', data: result});
    }
  });
});

router.post('/admin_login', recaptcha.middleware.verify, function(req, res, next) {
  if (!req.recaptcha.error) {
      let password = req.body.password = crypto.createHash('sha256').update(req.body.password).digest('hex');
      Users.findOne({username: req.body.username, password: password}, function (err, result) {
        console.log( result )
        if (err) throw err;
        if (result == null){
          res.json({status: 'fail', data: 'Username atau password yang anda masukkan salah.'});
        }
        else{
          let token = "";
          // if( !result.sessions ){
            token = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
          // }
          let expDate = new Date();
          expDate.setDate(expDate.getDate() + 10);
          

          Users.findOneAndUpdate({_id : result._id}, {$set:{last_login: new Date(), sessions: {token:token, expired_date: expDate } } },  function(err, res2){
              if (err) throw err;
              result.sessions[0] = { token: token, expired_date: expDate };
              result.password = null;
              
              res.json({status : 'success', data: result});
          });
        }
      });
  }else{
    res.json({status: 'fail', data: req.recaptcha.error});
  }

});

router.post('/logout', function(req, res, next) {
  let token = req.header('api_token');
  Otentikasi(token, function (result){
    if (result){
      Users.update({_id : result.user_id},
          {$pull : {sessions : {token : token}}},
          function (err, result2) {
            if (err) throw err;
            res.json({status : 'success', data : result2});
          });
    }
    else
    {
      res.json({status : 'denied', data : null});
    }
  });
});

module.exports = router;
