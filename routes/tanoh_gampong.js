var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var mongo = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');
moment.locale('ID');


var multer = require('multer');
var fs = require('fs');

let Users = require('../models/users.model');
let Otentikasi = require('../helpers/otentikasi');

// Models //
let Tanoh_gampong = require('../models/tanoh_gampong.model');

// Search
router.get('/search/', function(req, res, next) {
	let token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){

			Tanoh_gampong.find({ status : "publish" } , function (err, result) {
		        if (err) throw err;
		        let data    =  result;
		        console.log(data);
		        res.json( {
		            status: "success",
		            data: data
		        } );

		    }).limit(20);
		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});

// List untuk datatable
router.get('/list/', function(req, res, next) {
	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){
        	let keyword          = req.query.search;
		    let limit           = parseInt( req.query.limit );
			let offset = 0;
		    if( keyword !== null ){
		        offset = parseInt( req.query.offset );
		    }
		    let order           = req.query.order;
		    let order_column    = parseInt( req.query.order_column );

		    let status 	= req.query.status;

		    if( status === "trash" ){
		    	let total = await Tanoh_gampong.countDocuments({ $or: [{status : "trash"}] });
			    let search = new RegExp(keyword, 'i');
				Tanoh_gampong.find({ 
					$and: [
			          { $or: [{status : "trash"}] },
			          { nik: search, nama_subjek: search }
			        ]} , function (err, result) {
			        if (err) throw err;
			        let data    =  result;

			        res.json( {
			            recordsTotal: total,
			            recordsFiltered: limit,
			            data: data
			        } );
			    }).limit(limit).skip(offset).sort({_id:-1});
		    }else{
		    	let total = await Tanoh_gampong.countDocuments({ $or: [{status : "publish"}, {status : "draft"}] });
			    let search = new RegExp(keyword, 'i');
				Tanoh_gampong.find({ 
					$and: [
			          { $or: [{status : "publish"}, {status : "draft"}] },
			          { nik: search, nama_subjek: search }
			        ]} , function (err, result) {
			        if (err) throw err;
			        let data    =  result;

			        res.json( {
			            recordsTotal: total,
			            recordsFiltered: limit,
			            data: data
			        } );
			    }).limit(limit).skip(offset).sort({_id:-1});
		    }

        }
    });

});

router.post('/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[ 
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '20MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }, 
					{ name: 'foto_depan', maxSize: '5MB' },
					{ name: 'foto_belakang', maxSize: '5MB' },
					{ name: 'foto_kanan', maxSize: '5MB' },
					{ name: 'foto_kiri', maxSize: '5MB' },
					{ name: 'foto_atas', maxSize: '5MB' } 
				]);

			

			upload(req, res, function(err) {
				let foto_depan 		= ( req.files.foto_depan != undefined )?'upload/'+req.files.foto_depan[0].filename:null;
	        	let foto_belakang 	= ( req.files.foto_belakang != undefined )?'upload/'+req.files.foto_belakang[0].filename:null;
	        	let foto_kanan 		= ( req.files.foto_kanan != undefined )?'upload/'+req.files.foto_kanan[0].filename:null;
	        	let foto_kiri 		= ( req.files.foto_kiri != undefined )?'upload/'+req.files.foto_kiri[0].filename:null;
	        	let foto_atas 		= ( req.files.foto_atas != undefined )?'upload/'+req.files.foto_atas[0].filename:null;
	        	let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null;
	        	let file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null;

	        	let geolocation = null;
	        	if( file_geojson == null ){
	        		// jika koordinat dientri manual
		        	let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr } 
	        	}

		        if (req.fileValidationError) {

		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		// Simpan tanpa upload file
		        		let pre_data = {
		        			_id: new ObjectId,
		        			kode: Math.floor(Math.random() * 1000),
		        			nama_subjek: req.body.pemegang_hak,
						    nik: req.body.nik,
						    nomor_telepon: req.body.telepon,
						    tanda_bukti_hak: req.body.tanda_bukti,
						    status_tanah: req.body.status_tanah,
						    lokasi: req.body.lokasi,
						    jenis_tanah: req.body.jenis_tanah,
						    patok_tanda_batas: req.body.patok,
						    no_pbb: req.body.no_pbb,
						    status_pbb: req.body.status_pbb,
						    batas_utara: req.body.batas_utara,
						    batas_barat: req.body.batas_barat,
						    batas_selatan: req.body.batas_selatan,
						    batas_timur: req.body.batas_timur,
						    kelas_tanah: req.body.kelas_tanah,
						    papan_nama: req.body.papan_nama,
						    mutasi: req.body.mutasi,
						    riwayat_penguasaan_objek: req.body.riwayat_penugasan_objek,
						    kronologis_masalah: req.body.kronologis_masalah,
						    kronologis_penyelesaian: req.body.kronologis_penyelesaian,
							file_sertifikat: file_sertifikat,
							foto_depan: foto_depan,
							foto_belakang: foto_belakang,
							foto_kanan: foto_kanan,
							foto_kiri: foto_kiri,
							foto_atas: foto_atas,
							luas: req.body.luas,
							file_geojson: file_geojson,
							status: "publish",
							created_at: new Date(),
							updated_at: new Date()
		        		};

		        		Tanoh_gampong.create( pre_data ).then( msg => {
		        			res.json({status : 'success', data : pre_data._id});
		        		})

		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

		        	// Simpan dan upload file
		        	let pre_data = {
		        		_id: new ObjectId,
		        		kode: Math.floor(Math.random() * 1000),
	        			nama_subjek: req.body.pemegang_hak,
					    nik: req.body.nik,
					    nomor_telepon: req.body.telepon,
					    tanda_bukti_hak: req.body.tanda_bukti,
					    status_tanah: req.body.status_tanah,
					    lokasi: req.body.lokasi,
					    jenis_tanah: req.body.jenis_tanah,
					    patok_tanda_batas: req.body.patok,
					    no_pbb: req.body.no_pbb,
					    status_pbb: req.body.status_pbb,
					    batas_utara: req.body.batas_utara,
					    batas_barat: req.body.batas_barat,
					    batas_selatan: req.body.batas_selatan,
					    batas_timur: req.body.batas_timur,
					    kelas_tanah: req.body.kelas_tanah,
					    papan_nama: req.body.papan_nama,
					    mutasi: req.body.mutasi,
					    riwayat_penguasaan_objek: req.body.riwayat_penugasan_objek,
					    kronologis_masalah: req.body.kronologis_masalah,
					    kronologis_penyelesaian: req.body.kronologis_penyelesaian,
					    file_sertifikat: file_sertifikat,
						foto_depan: foto_depan,
						foto_belakang: foto_belakang,
						foto_kanan: foto_kanan,
						foto_kiri: foto_kiri,
						foto_atas: foto_atas,
						luas: req.body.luas,
						file_geojson: file_geojson,
						status: "publish",
						created_at: new Date(),
						updated_at: new Date()
	        		};

	        		Tanoh_gampong.create( pre_data ).then( msg => {
	        			res.json({status : 'success', data : pre_data._id});
	        		})
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/get/:id', function(req, res, next) {
	let token 		= req.header('api_token');
    let id = req.params.id;

	Otentikasi(token, async function (result){
        if (result){

		 const tanah = await Tanoh_gampong.find({_id: new ObjectId(id) });

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});

router.put('/edit/:id', function(req, res, next) {
    let token = req.header('api_token');
    let id = req.params.id;

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[ 
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '20MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }, 
					{ name: 'foto_depan', maxSize: '5MB' },
					{ name: 'foto_belakang', maxSize: '5MB' },
					{ name: 'foto_kanan', maxSize: '5MB' },
					{ name: 'foto_kiri', maxSize: '5MB' },
					{ name: 'foto_atas', maxSize: '5MB' } 
				]);

			
			upload(req, res, function(err) {
	        	let obj = {}
	        	if( req.files.file_geojson != undefined ){
	        		Object.assign(obj, {file_geojson: 'upload/'+req.files.file_geojson[0].filename})
	        	}

	        	if( req.files.file_sertifikat != undefined ){
	        		Object.assign(obj, {file_sertifikat: 'upload/'+req.files.file_sertifikat[0].filename})
	        	}

	        	if( req.files.foto_depan != undefined ){
	        		Object.assign(obj, {foto_depan: 'upload/'+req.files.foto_depan[0].filename})
	        	}

	        	if( req.files.foto_belakang != undefined ){
	        		Object.assign(obj, {foto_belakang: 'upload/'+req.files.foto_belakang[0].filename})
	        	}

	        	if( req.files.foto_kanan != undefined ){
	        		Object.assign(obj, {foto_kanan: 'upload/'+req.files.foto_kanan[0].filename})
	        	}

	        	if( req.files.foto_kiri != undefined ){
	        		Object.assign(obj, {foto_kiri: 'upload/'+req.files.foto_kiri[0].filename})
	        	}

	        	if( req.files.foto_atas != undefined ){
	        		Object.assign(obj, {foto_atas: 'upload/'+req.files.foto_atas[0].filename})
	        	}

	        	// Simpan dan upload file
	        	let pre_data = {
	        		kode: Math.floor(Math.random() * 1000),
        			nama_subjek: req.body.pemegang_hak,
				    nik: req.body.nik,
				    nomor_telepon: req.body.telepon,
				    tanda_bukti_hak: req.body.tanda_bukti,
				    status_tanah: req.body.status_tanah,
				    lokasi: req.body.lokasi,
				    jenis_tanah: req.body.jenis_tanah,
				    patok_tanda_batas: req.body.patok,
				    no_pbb: req.body.no_pbb,
				    status_pbb: req.body.status_pbb,
				    batas_utara: req.body.batas_utara,
				    batas_barat: req.body.batas_barat,
				    batas_selatan: req.body.batas_selatan,
				    batas_timur: req.body.batas_timur,
				    kelas_tanah: req.body.kelas_tanah,
				    papan_nama: req.body.papan_nama,
				    mutasi: req.body.mutasi,
				    riwayat_penguasaan_objek: req.body.riwayat_penugasan_objek,
				    kronologis_masalah: req.body.kronologis_masalah,
				    kronologis_penyelesaian: req.body.kronologis_penyelesaian,
				    luas: req.body.luas,
					updated_at: new Date()
        		};

        		let input = Object.assign(obj, pre_data);

		        if (req.fileValidationError) {
		        	// validation error
		        	res.json({status : 'fail', data : req.fileValidationError});
		        }else if (!req.files) {
		        	if( !err ){
		        		// update data
		        		Tanoh_gampong.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
			                if (err) throw err;

			                res.json({status : 'success', data : true});
			            });


		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		        	console.log( err )
		            res.json({status : 'fail', data : err});
		        }else{

	        		Tanoh_gampong.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
		                if (err) throw err;

		                res.json({status : 'success', data : true});
		            });
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.delete('/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    let status = req.query.status;

    Otentikasi(token, function (result){
        if (result){
        	Tanoh_gampong.find({ _id : new ObjectId(id)} , function (err, result) {
		        if (err) throw err;
		        let data    =  result;
		        if( result ){

		        	if( status == "permanent" ){
		        		/* Hapus Permanent */
		        		// Hapus file
						fs.unlink("public/"+result[0].file_sk, function (err) {}); 
						fs.unlink("public/"+result[0].file_geojson, function (err) {}); 

						Tanoh_gampong.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
			                if (err) throw err;
			                res.json({status: 'success', data: true});
			            });
		        	}else{
		        		/* Masukkan Tempat Sampah  */
		        		Tanoh_gampong.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
							if (err) throw err;

							res.json({status : 'success', data : true});
						});
		        	}
		        }
		        
		    }).limit(1);
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

router.post('/publish/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
        	let status = (req.body.status == "publish" )?"publish":"draft";

            Tanoh_gampong.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: status}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

module.exports = router;