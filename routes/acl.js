var express = require('express');
var router = express.Router();
var mongo = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var crypto = require('crypto');


var multer = require('multer');
var fs = require('fs');

let Users = require('../models/users.model');
let Modules = require('../models/modules.model');
let Groups = require('../models/groups.model');
let Otentikasi = require('../helpers/otentikasi');

//awal bagian users

//disini untuk fungsi datatable
router.get('/users/get_users/', function(req, res, next) {

    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Users.find({}, function (err, result) {
                if (err) throw err;
                var data =  result;
                res.json( {
                    recordsTotal: result.length,
                    recordsFiltered: 10,
                    data: data
                } );
            });
        }else{
            res.json({status : 'denied', data : null});
        }

    });
});

router.get('/users/list/', function(req, res, next) {
    let bandara = Users.find({}).find((error, doc) => {
      if (error) console.log(error);
      res.json({status : 'success', data : doc });
     }).sort({nama:1});

});

router.get('/users/get_users/:id', function(req, res, next) {
    let user_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Users.findOne({_id: new ObjectId(user_id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});

            });
        }
         else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

router.post('/users/add', function(req, res, next) {
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){

            // Upload
            let storage = multer.diskStorage({
                destination: function (req, file, cb) {
                  cb(null, 'public/upload/files')
                },
                filename: function (req, file, cb) {
                  let fileObj = {
                    "application/pdf": ".pdf"
                  };

                  if (fileObj[file.mimetype] == undefined) {
                    cb( "Format file tidak valid" );
                  } else {
                    cb(null, Date.now() + fileObj[file.mimetype])
                  }
                }
            })

            let upload = multer({ storage: storage }).single('sk');
            upload(req, res, function(err) {

                // Check username, email dan nik
                Users.findOne({$or:[{username : req.body.username}, {email : req.body.email}, {nik : req.body.nik}]}, function (err, user){
                    if (user!= null){
                        res.json({status : 'failed', data : 'Username atau email sudah digunakan'});
                    }else{
                        if (req.fileValidationError) {
                            res.json({status : 'fail', data : req.fileValidationError});
                        }
                        else if (!req.file) {
                            res.json({status : 'fail', data : err});
                        }
                        else if (err instanceof multer.MulterError) {
                            res.json({status : 'fail', data : err});
                        }else{
                            // Insert dan upload file
                            req.body.sk = 'upload/files/'+req.file.filename;
                            req.body.password = crypto.createHash('sha256').update(req.body.password).digest('hex');
                            
                            // req.body.sessions: {token:null, expired_date: null };

                            Users.create( req.body ).then( msg => {
                                res.json({status : 'success', data : "Data telah tersimpan"});
                            })
                        }
                    }
                    
                });
                
            });

        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

function updateUser(params, user, res)
{
    if( params.password != undefined ){
        params.password = crypto.createHash('sha256').update(params.password).digest('hex')
    }
    params.updated_at = new Date();
    Users.findOneAndUpdate({_id : user._id}, {$set:params},  function(err, res1){
        if (err) throw err;
        res.json({status : 'success', data : true});
    });
}

router.put('/users/edit/:id', function(req, res, next) {
    let user_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            // Upload
            let storage = multer.diskStorage({
                destination: function (req, file, cb) {
                  cb(null, 'public/upload/files')
                },
                filename: function (req, file, cb) {
                  let fileObj = {
                    "application/pdf": ".pdf"
                  };

                  if (fileObj[file.mimetype] == undefined) {
                    cb( "Format file tidak valid" );
                  } else {
                    cb(null, Date.now() + fileObj[file.mimetype])
                  }
                }
            })

            let upload = multer({ storage: storage }).single('sk');
            upload(req, res, function(err) {
                Users.findOne({_id : new ObjectId(user_id)}, function (err, user){
                    if (user!= null){

                        if (req.fileValidationError) {
                            res.json({status : 'fail', data : req.fileValidationError});
                        }
                        else if (!req.file) {
                            // Update tanpa upload file
                            updateUser(req.body, user, res);
                        }
                        else if (err instanceof multer.MulterError) {
                            res.json({status : 'fail', data : err});
                        }else{
                            // Insert dan upload file
                            req.body.sk = 'upload/files/'+req.file.filename;
                            req.body.password = crypto.createHash('sha256').update(req.body.password).digest('hex');
                            
                            

                            updateUser(req.body, user, res);
                        }
                    }else{
                        res.json({status : 'fail', data : "User tidak ditemukan"});
                    }
                    
                });
                
            });
            
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.put('/users/change_password/:id', function(req, res, next) {
    let user_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            // req.body.old_password = crypto.createHash('sha256').update(req.body.old_password).digest('hex');
            Users.findOne({_id : new ObjectId(user_id)}, function (err, cUser){

                if (cUser !== null)
                {
                    req.body.updated_at = new Date();
                    req.body.password = crypto.createHash('sha256').update(req.body.password).digest('hex');
                    Users.findOneAndUpdate({_id : cUser._id}, {$set:req.body},  function(err, res1){
                        if (err) throw err;
                        res.json({status : 'success', data : "Password berhasil diganti"});
                    });
                }
                else{
                    if (cUser._id !== user_id){
                        res.json({status : 'failed', data : 'User tidak ditemukan'});

                    }
                }
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

router.delete('/users/delete/:id', function(req, res, next) {
    let user_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Users.findOneAndDelete({_id: new ObjectId(user_id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

//akhir bagian users


//ini untuk bagian datatable
router.get('/modules/get_modules', function (req, res, next){
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        Modules.find({}, function (err, result) {
            if (err) throw err;
            var data =  result;
            res.json( {
                recordsTotal: result.length,
                recordsFiltered: 10,
                data: data
            } );
        });
    });
});

router.get('/modules/get_modules/:id', function (req, res, next) {
    let module_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Modules.findOne({_id: new ObjectId(module_id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/modules/get_modules_by_group_id/:id', function (req, res, next) {
    let group_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Groups.findOne({_id: new ObjectId(group_id)}, async function (err, result) {
                if (err) throw err;

                let output = await [];
                if(result){
                  let response_modules = result.modules;

                  for( let i in  response_modules ){
                    if( response_modules[i] != null )

                    await Modules.findOne({_id: new ObjectId(response_modules[i].module_id)}, function (mod_err, mod_result) {
                        if (mod_err) throw mod_err;

                        let group_subm  = response_modules[i].submodules;
                        let subm        = mod_result.submodules;
                        let submodules_filtered = [];

                        for( let j in group_subm ){
                            if( group_subm[j] != null ){
                                let find_sub = subm.find(sm => sm.submodule_id == group_subm[j].submodules_id.toString() );
                                submodules_filtered.push( find_sub );

                                // console.log( find_sub )
                            }
                            
                        }
                        mod_result.submodules = null;

                        output.push( 
                            Object.assign(
                                { 
                                    module_name: mod_result.module_name,
                                    module_url: mod_result.module_url,
                                    module_icon: mod_result.module_icon,
                                    is_active: mod_result.is_active

                                }, {submodules: submodules_filtered } 
                            )
                        )
                    }).sort({_id:-1});

                  }
                }
                res.json({status : 'success', data : output});

            });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.post('/modules/add_module', function (req, res, next){
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            req.body._id = new ObjectId();
            var module = new Modules(req.body);
            module.save().then(module =>{
                res.json({status : 'success', data : true});
            }).catch(err =>{
                throw err;
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.put('/modules/edit_module/:id', function (req, res, next) {
    let module_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Modules.findOneAndUpdate({_id : new ObjectId(module_id)}, {$set:req.body}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.delete('/modules/delete_module/:id', function (req, res, next) {
    let module_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Modules.findOneAndDelete({_id : new ObjectId(module_id)}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/modules/:module_id/get_submodules', function (req, res, next){
    let module_id = req.params.module_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result) {
        Modules.findOne({_id : new ObjectId(module_id)},function (err, result) {
            if (err) throw err;
            var data = result.submodules;

            if( typeof data == 'object' || data != null ){
                res.json( {
                    recordsTotal: data.length,
                    recordsFiltered: 10,
                    data: data
                } );
            }else{
                res.json( {
                    recordsTotal: 0,
                    recordsFiltered: 10,
                    data: "No result"
                } );
            }
        });

    });
});

router.get('/modules/:module_id/get_submodules/:submodule_id', function (req, res, next) {
    let module_id = req.params.module_id;
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Modules.findOne({"modules.id" : submodule_id}, function (err, result1){
                if (err) throw err;

                let submodule = result1.submodules.find(sm => sm._id == new ObjectId(submodule_id));
                res.json({status : 'success', data : submodule});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.post('/modules/:module_id/add_submodule', function (req, res, next) {
    let module_id = req.params.module_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            req.body.submodule_id = new ObjectId();
            Modules.update({_id : new ObjectId(module_id)}, {$push : {submodules : req.body} },
                function (err, result2) {
                    if (err) throw err;

                    res.json({status : 'success', data : true});

                });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.put('/modules/:module_id/edit_submodule/:submodule_id', function (req, res, next) {
    let module_id = req.params.module_id;
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            req.body.submodule_id = new ObjectId(submodule_id);
            Modules.updateOne({_id : new ObjectId(module_id), "submodules.submodule_id" : new ObjectId(submodule_id)},
                {$set : {"submodules.$" : req.body}} ,
                function (err, result2) {
                    if (err) throw err;

                    res.json({status : 'success', data : true});
                });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.delete('/modules/:module_id/delete_submodule/:submodule_id', function (req, res, next) {
    let module_id = req.params.module_id;
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Modules.update({_id : new ObjectId(module_id)},
                {$pull : {submodules : {submodule_id : new ObjectId(submodule_id)}}},
                function (err, result2) {
                    console.log(submodule_id);
                    if (err) throw err;

                    res.json({status : 'success', data : result2});
                });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});


router.get('/modules/get_submodules_by_group_id', function (req, res, next) {
    let module_id = req.params.module_id;
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            res.json({status : 'success', data : {} });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

//akhir bagian modules

//awal dari bagain crud submodules



//akhir dari bagian crud submodules

//awal bagian groups

router.get('/groups/get_groups', function (req, res, next) {
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        Groups.find({}, function (err, result) {
            if (err) throw err;
            var data =  result;
            res.json( {
                recordsTotal: result.length,
                recordsFiltered: 10,
                data: data
            } );
        });
    });

});

router.get('/groups/get_groups/:id', function (req, res, next) {
    let group_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Groups.findOne({_id : new ObjectId(group_id)},function (err, result2) {
                if (err) throw err;

                res.json({status : 'success', data : result2});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.post('/groups/add_group', function (req, res, next) {
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            req.body._id = new ObjectId();
            var group = new Groups(req.body);
            group.save().then(group =>{
                res.json({status : 'success', data : req.body._id});
                }).catch(err =>{
                throw err;
            });
        }
        else{
            res.json({status : 'denied', data : "Token Tidak Valid"});
        }
    });
});

router.put('/groups/edit_group/:id', function (req, res, next){
    let group_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Groups.findOneAndUpdate({_id : new ObjectId(group_id)}, req.body,function (err, result2) {
                if (err) throw err;

                res.json({status : 'success', data : result2});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.delete('/groups/delete_group/:id', function (req, res, next){
    let group_id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Groups.findOneAndDelete({_id : new ObjectId(group_id)}, function (err, result2) {
                if (err) throw err;

                res.json({status : 'success', data : result2});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});


router.get('/modules/get_modules_by_path/:id', function (req, res, next) {
    let group_id = req.params.id;
    let path          = req.query.path;

    let token = req.header('api_token');
    Otentikasi(token, async function (result){
        if (result){
            
            let submodules = await Modules.aggregate([
                {
                    $unwind :'$submodules'
                },
                {
                    $match : {'submodules.submodule_url': path }
                },
                { 
                    $project : { _id:0, submodule_id: '$submodules.submodule_id', submodule_url : '$submodules.submodule_url'} 
                }
            ]).limit(1).exec((err, submodule) => {
                if (err) throw err;

                let group = Groups.aggregate([
                    { 
                        $unwind :'$modules' 
                    },
                    { 
                        $unwind: "$modules.submodules" 
                    },
                    { 
                        $project : { module_id: '$modules.module_id', submodules: '$modules.submodules'} 
                    },
                    { 
                        $match : {_id: new ObjectId( group_id ), 'submodules.submodules_id': submodule[0].submodule_id } 
                    }

                ]).exec( (err, res2) =>{
                    res.json({status : 'success', data : res2});
                    // console.log( res2 );
                });
            });
            
            // res.json({status : 'success', data : submodules});
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

//akhir bagian groups


module.exports = router;
