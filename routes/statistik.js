var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var ObjectId = require('mongodb').ObjectID;

let Users = require('../models/users.model');
let Groups = require('../models/groups.model');
let Otentikasi = require('../helpers/otentikasi');
let Kabupaten = require('../models/kabupaten.model');
let Kecamatan = require('../models/kecamatan.model');
let Gampong = require('../models/desa.model');

router.get('/all', function(req, res, next) {
	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
	    if (result){
	    	// Users
	    	let jumlah_users = await Users.countDocuments({});

	    	// Groups
	    	let jumlah_group = await Groups.countDocuments({});

	    	// Kabupaten
	    	let jumlah_kabupaten = await Kabupaten.countDocuments({});

	    	// Kecamatan
	    	let jumlah_kecamatan = await Kecamatan.countDocuments({});

	    	// Kecamatan
	    	let jumlah_gampong = await Gampong.countDocuments({});

	    	res.json({status : 'success', data : {
	    		jumlah_users: jumlah_users,
	    		jumlah_group: jumlah_group-1, //kurang developer
	    		jumlah_kabupaten: jumlah_kabupaten,
	    		jumlah_kecamatan: jumlah_kecamatan,
	    		jumlah_gampong: jumlah_gampong
	    		
	    	}});

	    }else{
	        res.json({status : 'denied', data : "Token tidak valid!"});
	    }

	});
});

module.exports = router;
