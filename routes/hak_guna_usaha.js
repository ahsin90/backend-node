var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var mongo = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');
moment.locale('ID');


var multer = require('multer');
var fs = require('fs');

let Users = require('../models/users.model');
let Otentikasi = require('../helpers/otentikasi');

// Models //
let Hak_guna_usaha 				= require('../models/hak_guna_usaha.model');


/* -------- Kode -------- */
const kode = {
	hak_guna_usaha: 13,
}



// Ambil nomor terakhir urut untuk kode pertanahan
router.get('/nomor_urut/', function(req, res, next) {

    Hak_guna_usaha.countDocuments({}, function (err, result) {
        if (err) throw err;

        res.json( { data: {kode_pertanahan: kode.hak_guna_usaha, urut:result+1} } );
    });
});

// Search
router.get('/search/', function(req, res, next) {
	let token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){

        let keyword = new RegExp(req.query.keyword, 'i');
		const tanah = await Hak_guna_usaha.aggregate([
			   {
			     $lookup:
			       {
			         from: "kabupaten",
			         localField: "kabupaten_id",
			         foreignField: "kabupaten_id",
			         as: "kabupaten"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "kecamatan",
			         localField: "kecamatan_id",
			         foreignField: "kecamatan_id",
			         as: "kecamatan"
			       }
			  }
			]).match({nama:  keyword, status: "publish"}).limit(20);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}
		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});


// List untuk datatable
router.get('/list/', function(req, res, next) {
	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){
        	let keyword          = req.query.search;
		    let limit           = parseInt( req.query.limit );
			let offset = 0;
		    if( keyword !== null ){
		        offset = parseInt( req.query.offset );
		    }
		    let order           = req.query.order;
		    let order_column    = parseInt( req.query.order_column );

		    let status 			= req.query.status;
		    if( status === "trash" ){
		    	let total = await Hak_guna_usaha.countDocuments({ $or: [{status : "trash"}] });
			    let search = new RegExp(keyword, 'i');
				Hak_guna_usaha.find({ 
					$and: [
			          { $or: [{status : "trash"}] },
			          { kode: search, nama: search }
			        ]} , function (err, result) {
			        if (err) throw err;
			        let data    =  result;

			        res.json( {
			            recordsTotal: total,
			            recordsFiltered: limit,
			            data: data
			        } );
			    }).limit(limit).skip(offset).sort({_id:-1});
		    }else{
		    	let total = await Hak_guna_usaha.countDocuments({ $or: [{status : "publish"}, {status : "draft"}] });
			    let search = new RegExp(keyword, 'i');
				Hak_guna_usaha.find({ 
					$and: [
			          { $or: [{status : "publish"}, {status : "draft"}] },
			          { kode: search, nama: search }
			        ]} , function (err, result) {
			        if (err) throw err;
			        let data    =  result;

			        res.json( {
			            recordsTotal: total,
			            recordsFiltered: limit,
			            data: data
			        } );
			    }).limit(limit).skip(offset).sort({_id:-1});
		    }

        }
    });

});


// get single record
router.get('/get/:id', function(req, res, next) {
	let token 		= req.header('api_token');
    let id = req.params.id;

	Otentikasi(token, async function (result){
        if (result){

		 const tanah = await Hak_guna_usaha.aggregate([
			   {
			     $lookup:
			       {
			         from: "kabupaten",
			         localField: "kabupaten_id",
			         foreignField: "kabupaten_id",
			         as: "kabupaten"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "kecamatan",
			         localField: "kecamatan_id",
			         foreignField: "kecamatan_id",
			         as: "kecamatan"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "desa",
			         localField: "desa_id",
			         foreignField: "desa_id",
			         as: "desa"
			       }
			  }
			]).match({_id: new ObjectId(id) }).limit(1);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}


		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});


router.post('/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sk', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }
				]);

			

			upload(req, res, function(err) {
				let file_sk = ( req.files.file_sk != undefined )?'upload/'+req.files.file_sk[0].filename:null;
	        	let file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null;

	        	let geolocation = null;
	        	if( file_geojson == null ){
	        		// jika koordinat dientri manual
		        	let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr } 
	        	}

		        if (req.fileValidationError) {

		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		// Simpan tanpa upload file
		        		let pre_data = {
		        			_id: new ObjectId,
		        			kode: req.body.kode,
		        			kabupaten_id: req.body.kabupaten_id,
		        			kecamatan_id: req.body.kecamatan_id,
		        			desa_id: req.body.desa_id,
		        			nomor_sk: req.body.nomor_sk,
		        			tanggal_sk: req.body.tanggal_sk,
		        			tanggal_berakhir: req.body.tanggal_berakhir,
		        			file_sk: file_sk,
		        			luas: req.body.luas,
		        			nama: req.body.nama,
		        			peruntukan: req.body.peruntukan,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "draft",
							created_at: new Date(),
							updated_at: new Date()

		        		};


		        		Hak_guna_usaha.create( pre_data ).then( msg => {
		        			res.json({status : 'success', data : pre_data._id});
		        		})

		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

		        	// Simpan dan upload file
		        	let pre_data = {
		        		_id: new ObjectId,
	        			kode: req.body.kode,
	        			kabupaten_id: req.body.kabupaten_id,
	        			kecamatan_id: req.body.kecamatan_id,
	        			desa_id: req.body.desa_id,
	        			nomor_sk: req.body.nomor_sk,
	        			tanggal_sk: req.body.tanggal_sk,
	        			tanggal_berakhir: req.body.tanggal_berakhir,
	        			file_sk: file_sk,
	        			luas: req.body.luas,
	        			nama: req.body.nama,
	        			peruntukan: req.body.peruntukan,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "draft",
						created_at: new Date(),
						updated_at: new Date()
	        		};

	        		Hak_guna_usaha.create( pre_data ).then( msg => {
	        			res.json({status : 'success', data : pre_data._id});
	        		})
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/edit/:id', function(req, res, next) {
    let token = req.header('api_token');
    let id = req.params.id;

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sk', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }
				]);

			

			upload(req, res, function(err) {
	        	let obj = {}
	        	if( req.files.file_geojson != undefined ){
	        		Object.assign(obj, {file_geojson: 'upload/'+req.files.file_geojson[0].filename})
	        	}

	        	if( req.files.file_sk != undefined ){
	        		Object.assign(obj, {file_sk: 'upload/'+req.files.file_sk[0].filename})
	        	}

	        	let pre_data = {
        			kabupaten_id: req.body.kabupaten_id,
        			kecamatan_id: req.body.kecamatan_id,
        			desa_id: req.body.desa_id,
        			nomor_sk: req.body.nomor_sk,
        			tanggal_sk: req.body.tanggal_sk,
        			tanggal_berakhir: req.body.tanggal_berakhir,
        			luas: req.body.luas,
        			nama: req.body.nama,
        			peruntukan: req.body.peruntukan,
					status: "draft",
					updated_at: new Date()
        		};

        		let input = Object.assign(obj, pre_data)


		        if (req.fileValidationError) {
		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		Hak_guna_usaha.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
			                if (err) throw err;

			                res.json({status : 'success', data : true});
			            });


		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		        	console.log( err )
		            res.json({status : 'fail', data : err});
		        }else{

	        		Hak_guna_usaha.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
		                if (err) throw err;

		                res.json({status : 'success', data : true});
		            });
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.post('/publish/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
        	let status = (req.body.status == "publish" )?"publish":"draft";

            Hak_guna_usaha.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: status}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});


// Delete
router.delete('/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    let status = req.query.status;
    Otentikasi(token, function (result){
        if (result){
        	Hak_guna_usaha.find({ _id : new ObjectId(id)} , function (err, result) {
		        if (err) throw err;
		        let data    =  result;
		        if( result ){

		        	if( status == "permanent" ){
		        		/* Hapus Permanent */
		        		// Hapus file
						fs.unlink("public/"+result[0].file_sk, function (err) {}); 
						fs.unlink("public/"+result[0].file_geojson, function (err) {}); 

						Hak_guna_usaha.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
			                if (err) throw err;
			                res.json({status: 'success', data: true});
			            });
		        	}else{
		        		/* Masukkan Tempat Sampah  */
		        		Hak_guna_usaha.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
							if (err) throw err;

							res.json({status : 'success', data : true});
						});
		        	}
		        }
		        
		    }).limit(1);
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});


module.exports = router;
