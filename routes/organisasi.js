var express = require('express');
var router = express.Router();
var crypto = require('crypto');

var ObjectId = require('mongodb').ObjectID;

let Users = require('../models/users.model');
let Otentikasi = require('../helpers/otentikasi');
let Opd = require('../models/opd.model');

router.get('/list_skpa/', function(req, res, next) {
    Opd.find({ level: "Provinsi"} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
                        status: "success",
            data: data
        } );
    }).sort({nama_opd:1});

});

// search
router.get('/search/', async function(req, res, next) {
    let keyword          = req.query.keyword;

    let search = new RegExp(keyword, 'i');
    Opd.find({ nama_opd: search} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            status: "success",
            data: data
        } );
    }).limit(20);

});

router.get('/get_opd_list_by_wilayah/:id', function(req, res, next) {
    const token         = req.header('api_token');
    const kab_id      = req.params.id;

    Opd.find({kabupaten_id: kab_id}).find((error, doc) => {
      if (error) console.log(error);
      res.json({status : 'success', data : doc });
     }).sort({nama_opd:1});

});



router.get('/list/', async function(req, res, next) {
	const token 		= req.header('api_token');
    let level       = "";
    if( req.query.kabupaten ){
        level = "Kabupaten"
    }else{
        level = "Provinsi"
    }

    let keyword          = req.query.search;
    let limit           = parseInt( req.query.limit );
	let offset = 0;
    if( keyword !== null ){
        offset = parseInt( req.query.offset );
    }
    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Opd.count();

    let search = new RegExp(keyword, 'i');
		Opd.find({ nama_opd: search, level: level } , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            recordsTotal: total,
            recordsFiltered: limit,
            data: data
        } );
    }).limit(limit).skip(offset).sort({_id:-1});

});



router.post('/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
            let level       = "";
            if( req.query.kabupaten ){
                level = "Kabupaten"
            }else{
                level = "Provinsi"
            }
            req.body.level = level;
			let opd = new Opd( req.body );

			let coordinate = { type: "Point", coordinates: [ parseFloat(req.body.longitude), parseFloat( req.body.latitude ) ] } ;
			let location = {location: coordinate};

			let input = Object.assign(req.body, location);

			Opd.create( input ).then( msj => {
				res.json({status : 'success', data : true});
			})

        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});


router.get('/get/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Opd.findOne({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
         else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/edit/:id', function (req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
					let coordinate = { type: "Point", coordinates: [ parseFloat(req.body.longitude), parseFloat( req.body.latitude ) ] } ;
					let location = {location: coordinate};
					let input = Object.assign(req.body, location);

          Opd.findOneAndUpdate({_id : new ObjectId(id)}, {$set: input}, function (err, result1){
              if (err) throw err;

              res.json({status : 'success', data : true});
          });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.delete('/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Opd.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

module.exports = router;
