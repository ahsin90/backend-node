var express = require('express'),
	router = express.Router(),
	crypto = require('crypto'),
	mongo = require('mongodb').MongoClient,
	ObjectId = require('mongodb').ObjectID,
	moment = require('moment'),
	multer = require('multer'),
	fs = require('fs');

moment.locale('ID');

let Users = require('../models/users.model'),
	Otentikasi = require('../helpers/otentikasi');

// Models //
let Batas_provinsi 	= require('../models/batas_provinsi.model'),
	Batas_kabupaten	= require('../models/batas_kabupaten.model'),
	Batas_kecamatan	= require('../models/batas_kecamatan.model'),
	Batas_desa		= require('../models/batas_desa.model');

/**
 * Batas Wilayah -- Provinsi
 *
 * @router list
 * @router add
 * @router delete
 * @router detail
 *
 * @return
 */


/* -------- Kode -------- */
const kode = {
	batas_wilayah: 2,
}

// Ambil nomor terakhir urut untuk kode pertanahan
router.get('/nomor_urut/', function(req, res, next) {

    Batas_kabupaten.countDocuments({}, function (err, result) {
        if (err) throw err;

        res.json( { data: {kode_batas_wilayah: kode.batas_wilayah, urut:result+1} } );
    });
});

// Search
router.get('/batas_provinsi/search/', function(req, res, next) {
	let token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){

        let keyword = new RegExp(req.query.keyword, 'i');
		const tanah = await Batas_provinsi.aggregate([]).match({status: "publish"}).limit(1);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}
		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});

// List Batas Wilayah - Provinsi
router.get('/batas_provinsi/list/', function(req, res, next) {

	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
		if (result){
			let keyword          = req.query.search;
		    let limit           = parseInt( req.query.limit );
			let offset = 0;
		    if( keyword !== null ){
		        offset = parseInt( req.query.offset );
		    }
		    let order           = req.query.order;
		    let order_column    = parseInt( req.query.order_column );

		    let status 			= req.query.status; 

		    let total = await Batas_provinsi.count();

		    let search = new RegExp(keyword, 'i');

		    let BP = null;
		    if( status === "trash" ){
		    	BP = await Batas_provinsi.aggregate([
					{
						$lookup:
							{
								from: 'provinsi',
								localField: 'provinsi_id',
								foreignField: 'provinsi_id',
								as:'provinsi'
							},
					},
					{ 
						$match : 
						{ 
							status: "trash"
						} 
					} 
				]);
		    }else{
		    	BP = await Batas_provinsi.aggregate([
					{
						$lookup:
							{
								from: 'provinsi',
								localField: 'provinsi_id',
								foreignField: 'provinsi_id',
								as:'provinsi'
							},
					},
					{ 
						$match : 
						{ 
							$and: [
					          { $or: [{status : "publish"}, {status : "draft"}] }
					        ]
						} 
					} 
				]);
		    }
	    	


			res.json( {
	            recordsTotal: total,
	            recordsFiltered: limit,
	            data: BP
	        } );

		}
	});

});
// End List Batas Wilayah - Provinsi

// Add Batas Wilayah - Provinsi
router.post('/batas_provinsi/add', function(req, res, next) {
	let token = req.header('api_token');

	Otentikasi(token, function (result) {
		if (result) {

			// Upload file
			let storage = multer.diskStorage({
				destination: function (req, file, cb) {
					cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
					let fileObj = {
						"application/pdf": ".pdf",
						"application/json": ".json",
						"application/geo+json": ".geojson",
						"image/png": ".png",
						"image/jpeg": ".jpeg",
						"image/jpg": ".jpg"
					};

					if (fileObj[file.mimetype] == undefined) {
						cb( "Jenis file tidak valid" );
					} else {
						cb(null, Date.now() + fileObj[file.mimetype])
					}
				}
			});

			let upload	= multer({ storage: storage }).fields([
				{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' },
				{ name: 'file_geojson', maxCount: 1, maxSize: '2MB' },
			]);

			upload(req, res, function(err) {
				let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null,
					file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null,
					geolocation = null;

				if( file_geojson == null ){
					// jika koordinat dientri manual
					let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr }
				}

				if (req.fileValidationError) {

					res.json({status : 'fail', data : req.fileValidationError});

				}else if (!req.files) {
					if( !err ){

						// Simpan tanpa upload file
						let pre_data = {
							_id: new ObjectId,
							provinsi_id: req.body.provinsi_id,
							nama: "Batas Wilayah Provinsi Aceh",
							nomor_izin: req.body.nomor_izin,
							tanggal_berita_acara: req.body.tanggal_berita_acara,
							nomor_pilar: req.body.nomor_pilar,
							luas: req.body.luas,
							file_sertifikat: null,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "publish"
						};

						Batas_provinsi.create( pre_data ).then( msg => {
							res.json({status : 'success', data : pre_data._id});
						})

					}else{
						res.json({status : 'fail', data : err});
					}
				}
				else if (err instanceof multer.MulterError) {
					res.json({status : 'fail', data : err});
				}else{

					// Simpan dan upload file
					let pre_data = {
						_id: new ObjectId,
						provinsi_id: req.body.provinsi_id,
						nama: "Batas Wilayah Provinsi Aceh",
						nomor_izin: req.body.nomor_izin,
						tanggal_berita_acara: req.body.tanggal_berita_acara,
						nomor_pilar: req.body.nomor_pilar,
						luas: req.body.luas,
						file_sertifikat: file_sertifikat,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "publish"
					};

					Batas_provinsi.create( pre_data ).then( msg => {
						res.json({status : 'success', data : pre_data._id});
					})
				}

			});

		}
	});
});
// End Add Batas Wilayah - Provinsi

// Batas provinsi publish/draft
router.post('/batas_provinsi/publish/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
        	let status = (req.body.status == "publish" )?"publish":"draft";

            Batas_provinsi.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: status}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});
// End batas provinsi publish/draft

// Batas provinsi edit
router.put('/batas_provinsi/edit/:id', function(req, res, next) {
    let token = req.header('api_token');
    let id = req.params.id;

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '2MB' }
				]);

			

			upload(req, res, function(err) {
	        	let obj = {}
	        	if( req.files.file_geojson != undefined ){
	        		Object.assign(obj, {file_geojson: 'upload/'+req.files.file_geojson[0].filename})
	        	}

	        	if( req.files.file_sertifikat != undefined ){
	        		Object.assign(obj, {file_sertifikat: 'upload/'+req.files.file_sertifikat[0].filename})
	        	}

	        	let pre_data = {
					provinsi_id: req.body.provinsi_id,
					nama: "Batas Wilayah Provinsi Aceh",
					nomor_izin: req.body.nomor_izin,
					tanggal_berita_acara: req.body.tanggal_berita_acara,
					nomor_pilar: req.body.nomor_pilar,
					luas: req.body.luas,
					status: "publish",
					updated_at: new Date()
        		};

        		let input = Object.assign(obj, pre_data)

        		console.log( input )



		        if (req.fileValidationError) {
		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		Batas_provinsi.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
			                if (err) throw err;

			                res.json({status : 'success', data : true});
			            });


		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		        	console.log( err )
		            res.json({status : 'fail', data : err});
		        }else{

	        		Batas_provinsi.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
		                if (err) throw err;

		                res.json({status : 'success', data : true});
		            });
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


// Batas Provinsi Delete
router.delete('/batas_provinsi/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    let status 	= req.query.status;

    Otentikasi(token, function (result){
        if (result){
        	Batas_provinsi.find({ _id : new ObjectId(id)} , function (err, result) {
		        if (err) throw err;
		        let data    =  result;
		        if( result ){

		        	if( status == "permanent" ){
		        		/* Hapus Permanent */
		        		// Hapus file
						fs.unlink("public/"+result[0].file_sertifikat, function (err) {}); 
						fs.unlink("public/"+result[0].file_geojson, function (err) {}); 

						Batas_provinsi.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
			                if (err) throw err;
			                res.json({status: 'success', data: true});
			            });
		        	}else{
		        		/* Masukkan Tempat Sampah  */
		        		Batas_provinsi.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
							if (err) throw err;

							res.json({status : 'success', data : true});
						});
		        	}
		        }
		        
		    }).limit(1);
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});


// Detail Batas Wilayah - Provinsi
// get single record
router.get('/batas_provinsi/get/:id', function(req, res, next) {
	let token	= req.header('api_token'),
		id = req.params.id;

	Otentikasi(token, async function (result){
		if (result){

			const BP = await Batas_provinsi.aggregate([
				{
					$lookup:
						{
							from: "provinsi",
							localField: "provinsi_id",
							foreignField: "provinsi_id",
							as: "provinsi"
						}
				},

			]).match({_id: new ObjectId(id) }).limit(1);

			if( BP.length > 0 ){
				res.json({status: 'success', data: BP[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}
		}else{
			res.json({status : 'denied', data : "Token tidak valid"});
		}
	});

});
// End Detail Batas Wilayah - Provinsi


/**
 * Batas Wilayah -- Kabupaten/Kota
 *
 * @router list
 * @router add
 * @router delete
 * @router detail
 *
 * @return
 */

// Search
router.get('/batas_kabupaten/search/', function(req, res, next) {
	let token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){

        let keyword = new RegExp(req.query.keyword, 'i');
		const tanah = await Batas_kabupaten.aggregate([]).match({nama:  keyword, status: "publish"}).limit(30);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}
		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});

// List Batas Wilayah - Kabupaten
router.get('/batas_kabupaten/list/', function(req, res, next) {

	const token	= req.header('api_token');

	Otentikasi(token, async function (result){
		if (result){
			let keyword          = req.query.search;
			let limit           = parseInt( req.query.limit );
			let offset = 0;
			if( keyword !== null ){
				offset = parseInt( req.query.offset );
			}
			let order           = req.query.order;
			let order_column    = parseInt( req.query.order_column );

			let status 			= null;
			if( req.query.status ){
				status = req.query.status;
			}else{
				status = "publish";
			}


			let total = await Batas_kabupaten.count();

			let search = new RegExp(keyword, 'i');

			let BK = null;
			if( status === "trash" ){
				BK = await Batas_kabupaten.aggregate([
					{
						$lookup:
							{
								from: 'kabupaten',
								localField: 'kabupaten_id',
								foreignField: 'kabupaten_id',
								as:'kabupaten'
							},
					},
					{ 
						$match : 
						{ 
							status : "trash"
					        
						} 
					} 
				]);
			}else{
				BK = await Batas_kabupaten.aggregate([
					{
						$lookup:
							{
								from: 'kabupaten',
								localField: 'kabupaten_id',
								foreignField: 'kabupaten_id',
								as:'kabupaten'
							},
					},
					{ 
						$match : 
						{ 
							$and: [
					          { $or: [{status : "publish"}, {status : "draft"}] }
					        ]
						} 
					} 
				]);
			}
			



			res.json( {
	            recordsTotal: total,
	            recordsFiltered: limit,
	            data: BK
	        } );
		}
	});


});
// End List Batas Wilayah - Kabupaten

// Add Batas Wilayah - Kabupaten
router.post('/batas_kabupaten/add', function(req, res, next) {
	let token = req.header('api_token');

	Otentikasi(token, function (result) {
		if (result) {

			// Upload file
			let storage = multer.diskStorage({
				destination: function (req, file, cb) {
					cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
					let fileObj = {
						"application/pdf": ".pdf",
						"application/json": ".json",
						"application/geo+json": ".geojson",
						"image/png": ".png",
						"image/jpeg": ".jpeg",
						"image/jpg": ".jpg"
					};

					if (fileObj[file.mimetype] == undefined) {
						cb( "Jenis file tidak valid" );
					} else {
						cb(null, Date.now() + fileObj[file.mimetype])
					}
				}
			});

			let upload	= multer({ storage: storage }).fields([
				{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' },
				{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' },
			]);

			upload(req, res, function(err) {
				let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null,
					file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null,
					geolocation = null;

				if( file_geojson == null ){
					// jika koordinat dientri manual
					let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr }
				}

				if (req.fileValidationError) {

					res.json({status : 'fail', data : req.fileValidationError});

				}else if (!req.files) {
					if( !err ){

						// Simpan tanpa upload file
						let pre_data = {
							_id: new ObjectId,
							provinsi_id: req.body.provinsi_id,
							nama: req.body.nama,
							kabupaten_id: req.body.kabupaten_id,
							nomor_izin: req.body.nomor_izin,
							tanggal_berita_acara: req.body.tanggal_berita_acara,
							nomor_pilar: req.body.nomor_pilar,
							luas: req.body.luas,
							warna_palette: req.body.warna_palette,
							file_sertifikat: null,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "publish"
						};

						Batas_kabupaten.create( pre_data ).then( msg => {
							res.json({status : 'success', data : pre_data._id});
						})

					}else{
						res.json({status : 'fail', data : err});
					}
				}
				else if (err instanceof multer.MulterError) {
					res.json({status : 'fail', data : err});
				}else{

					// Simpan dan upload file
					let pre_data = {
						_id: new ObjectId,
						provinsi_id: req.body.provinsi_id,
						nama: req.body.nama,
						kabupaten_id: req.body.kabupaten_id,
						nomor_izin: req.body.nomor_izin,
						tanggal_berita_acara: req.body.tanggal_berita_acara,
						nomor_pilar: req.body.nomor_pilar,
						luas: req.body.luas,
						warna_palette: req.body.warna_palette,
						file_sertifikat: file_sertifikat,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "publish"
					};

					Batas_kabupaten.create( pre_data ).then( msg => {
						res.json({status : 'success', data : pre_data._id});
					})
				}

			});

		}
	});
});
// End Add Batas Wilayah - Kabupaten

// Batas kabupaten edit
router.put('/batas_kabupaten/edit/:id', function(req, res, next) {
    let token = req.header('api_token');
    let id = req.params.id;

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '2MB' }
				]);

			

			upload(req, res, function(err) {
	        	let obj = {}
	        	if( req.files.file_geojson != undefined ){
	        		Object.assign(obj, {file_geojson: 'upload/'+req.files.file_geojson[0].filename})
	        	}

	        	if( req.files.file_sertifikat != undefined ){
	        		Object.assign(obj, {file_sertifikat: 'upload/'+req.files.file_sertifikat[0].filename})
	        	}

	        	let pre_data = {
	        		provinsi_id: req.body.provinsi_id,
	        		nama: req.body.nama,
					kabupaten_id: req.body.kabupaten_id,
					nomor_izin: req.body.nomor_izin,
					tanggal_berita_acara: req.body.tanggal_berita_acara,
					nomor_pilar: req.body.nomor_pilar,
					luas: req.body.luas,
					warna_palette: req.body.warna_palette,
					status: "publish",
					updated_at: new Date()
        		};

        		let input = Object.assign(obj, pre_data)


		        if (req.fileValidationError) {
		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		Batas_kabupaten.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
			                if (err) throw err;

			                res.json({status : 'success', data : true});
			            });


		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

	        		Batas_kabupaten.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
		                if (err) throw err;

		                res.json({status : 'success', data : true});
		            });
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});
// End edit batas kabupaten 

// Batas kabupaten publish
router.post('/batas_kabupaten/publish/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
        	let status = (req.body.status == "publish" )?"publish":"draft";

            Batas_kabupaten.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: status}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});
//  End batas kanupaten publish

// Delete Batas Wilayah - Kabupaten
router.delete('/batas_kabupaten/delete/:id', function(req, res, next) {
	let id 		= req.params.id;
	let status 	= req.query.status;
	let token 	= req.header('api_token');
	Otentikasi(token, function (result){
		if (result){
			Batas_kabupaten.find({ _id : new ObjectId(id)} , function (err, result) {
		        if (err) throw err;
		        let data    =  result;
		        if( result ){

		        	if( status == "permanent" ){
		        		/* Hapus Permanent */
		        		// Hapus file
						fs.unlink("public/"+result[0].file_sertifikat, function (err) {}); 
						fs.unlink("public/"+result[0].file_geojson, function (err) {}); 

						Batas_kabupaten.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
			                if (err) throw err;
			                res.json({status: 'success', data: true});
			            });
		        	}else{
		        		/* Masukkan Tempat Sampah  */
		        		Batas_kabupaten.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
							if (err) throw err;

							res.json({status : 'success', data : true});
						});
		        	}
		        }
		        
		    }).limit(1);
		}
		else
		{
			res.json({status : 'denied', data : null});
		}
	});
});


// Detail Batas Wilayah - Kabupaten
// get single record
router.get('/batas_kabupaten/get/:id', function(req, res, next) {
	let token	= req.header('api_token'),
		id = req.params.id;

	Otentikasi(token, async function (result){
		if (result){

			const BP = await Batas_kabupaten.aggregate([
				{
					$lookup:
						{
							from: "provinsi",
							localField: "provinsi_id",
							foreignField: "provinsi_id",
							as: "provinsi"
						}
				},
				{
					$lookup:
						{
							from: "kabupaten",
							localField: "kabupaten_id",
							foreignField: "kabupaten_id",
							as: "kabupaten"
						}
				},

			]).match({_id: new ObjectId(id) }).limit(1);

			if( BP.length > 0 ){
				res.json({status: 'success', data: BP[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}
		}else{
			res.json({status : 'denied', data : "Token tidak valid"});
		}
	});

});
// End Detail Batas Wilayah - Kabupaten

/**
 * Batas Wilayah -- Kecamatan
 *
 * @router list
 * @router add
 * @router delete
 * @router detail
 *
 * @return
 */

// List Batas Wilayah - Kecamatan
router.get('/batas_kecamatan/list/', function(req, res, next) {

	const token	= req.header('api_token');

	Otentikasi(token, async function (result){
		if (result){
			let keyword          = req.query.search;
			let limit           = parseInt( req.query.limit );
			let offset = 0;
			if( keyword !== null ){
				offset = parseInt( req.query.offset );
			}
			let order           = req.query.order;
			let order_column    = parseInt( req.query.order_column );

			let status 			= null;
			if( req.query.status ){
				status = req.query.status;
			}else{
				status = "publish";
			}

			let total = await Batas_kecamatan.count();

			let search = new RegExp(keyword, 'i');
			Batas_kecamatan.find({ nomor_izin: search, status: status } , function (err, result) {
				if (err) throw err;
				let data	=  result;

				res.json( {
					recordsTotal: total,
					recordsFiltered: limit,
					data: data
				} );
			}).limit(limit).skip(offset).sort({_id:-1});
		}
	});

});
// End List Batas Wilayah - Kecamatan

// Add Batas Wilayah - Kecamatan
router.post('/batas_kecamatan/add', function(req, res, next) {
	let token = req.header('api_token');

	Otentikasi(token, function (result) {
		if (result) {

			// Upload file
			let storage = multer.diskStorage({
				destination: function (req, file, cb) {
					cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
					let fileObj = {
						"application/pdf": ".pdf",
						"application/json": ".json",
						"application/geo+json": ".geojson",
						"image/png": ".png",
						"image/jpeg": ".jpeg",
						"image/jpg": ".jpg"
					};

					if (fileObj[file.mimetype] == undefined) {
						cb( "Jenis file tidak valid" );
					} else {
						cb(null, Date.now() + fileObj[file.mimetype])
					}
				}
			});

			let upload	= multer({ storage: storage }).fields([
				{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' },
				{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' },
			]);

			upload(req, res, function(err) {
				let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null,
					file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null,
					geolocation = null;

				if( file_geojson == null ){
					// jika koordinat dientri manual
					let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr }
				}

				if (req.fileValidationError) {

					res.json({status : 'fail', data : req.fileValidationError});

				}else if (!req.files) {
					if( !err ){

						// Simpan tanpa upload file
						let pre_data = {
							_id: new ObjectId,
							provinsi_id: req.body.provinsi_id,
							kabupaten_id: req.body.kabupaten_id,
							kecamatan_id: req.body.kecamatan_id,
							nomor_izin: req.body.nomor_izin,
							tanggal_berita_acara: req.body.tanggal_berita_acara,
							nomor_pilar: req.body.nomor_pilar,
							warna_palette: req.body.warna_palette,
							file_sertifikat: null,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "publish"
						};

						Batas_kecamatan.create( pre_data ).then( msg => {
							res.json({status : 'success', data : pre_data._id});
						})

					}else{
						res.json({status : 'fail', data : err});
					}
				}
				else if (err instanceof multer.MulterError) {
					res.json({status : 'fail', data : err});
				}else{

					// Simpan dan upload file
					let pre_data = {
						_id: new ObjectId,
						provinsi_id: req.body.provinsi_id,
						kabupaten_id: req.body.kabupaten_id,
						kecamatan_id: req.body.kecamatan_id,
						nomor_izin: req.body.nomor_izin,
						tanggal_berita_acara: req.body.tanggal_berita_acara,
						nomor_pilar: req.body.nomor_pilar,
						warna_palette: req.body.warna_palette,
						file_sertifikat: file_sertifikat,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "publish"
					};

					Batas_kecamatan.create( pre_data ).then( msg => {
						res.json({status : 'success', data : pre_data._id});
					})
				}

			});

		}
	});
});
// End Add Batas Wilayah - Kecamatan

// Edit Batas Wilayah - Kecamatan
// End Edit Batas Wilayah - Kecamatan

// Delete Batas Wilayah - Kecamatan
router.delete('/batas_kecamatan/delete/:id', function(req, res, next) {
	let id = req.params.id;
	let token = req.header('api_token');
	Otentikasi(token, function (result){
		if (result){
			Batas_kecamatan.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
				if (err) throw err;

				res.json({status : 'success', data : true});
			});
		}
		else
		{
			res.json({status : 'denied', data : null});
		}
	});
});
// End Delete Batas Wilayah - Kecamatan

// Detail Batas Wilayah - Kecamatan
// get single record
router.get('/batas_kecamatan/get/:id', function(req, res, next) {
	let token	= req.header('api_token'),
		id = req.params.id;

	Otentikasi(token, async function (result){
		if (result){

			const BKc = await Batas_kecamatan.aggregate([
				{
					$lookup:
						{
							from: "provinsi",
							localField: "provinsi_id",
							foreignField: "provinsi_id",
							as: "provinsi"
						}
				},
				{
					$lookup:
						{
							from: "kabupaten",
							localField: "kabupaten_id",
							foreignField: "kabupaten_id",
							as: "kabupaten"
						}
				},
				{
					$lookup:
						{
							from: "kecamatan",
							localField: "kecamatan_id",
							foreignField: "kecamatan_id",
							as: "kecamatan"
						}
				},

			]).match({_id: new ObjectId(id) }).limit(1);

			if( BKc.length > 0 ){
				res.json({status: 'success', data: BKc[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}
		}else{
			res.json({status : 'denied', data : "Token tidak valid"});
		}
	});

});
// End Detail Batas Wilayah - Kecamatan

module.exports = router;
