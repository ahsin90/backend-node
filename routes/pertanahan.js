var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var mongo = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');
moment.locale('ID');


var multer = require('multer');
var fs = require('fs');

let Users = require('../models/users.model');
let Otentikasi = require('../helpers/otentikasi');

// Models //
let Tanah_bangunan 				= require('../models/tanah_bangunan.model');
let Tanah_kas_gampong 			= require('../models/tanah_kas_gampong.model');
let Tanah_persil_masyarakat 	= require('../models/tanah_persil_masyarakat.model');
let Tanah_wakaf 				= require('../models/tanah_wakaf.model');

/* -------- Kode -------- */
const kode = {
	bangunan: 17,
	tanah_kas_gampong: 19,
	tanah_persil_masyarakat: 20,
	tanah_wakaf: 18
}

/* -------- Tanah Bangunan Pemerintah -------- */

// Ambil nomor terakhir urut untuk kode pertanahan
router.get('/bangunan/nomor_urut/', function(req, res, next) {

    Tanah_bangunan.countDocuments({}, function (err, result) {
        if (err) throw err;

        res.json( { data: {kode_pertanahan: kode.bangunan, urut:result+1} } );
    });
});

// Search
router.get('/bangunan/search/', function(req, res, next) {
	let token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){

        let keyword = new RegExp(req.query.keyword, 'i');
		const tanah = await Tanah_bangunan.aggregate([
			   {
			     $lookup:
			       {
			         from: "kabupaten",
			         localField: "kabupaten_id",
			         foreignField: "kabupaten_id",
			         as: "kabupaten"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "kecamatan",
			         localField: "kecamatan_id",
			         foreignField: "kecamatan_id",
			         as: "kecamatan"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "desa",
			         localField: "desa_id",
			         foreignField: "desa_id",
			         as: "desa"
			       }
			  }
			]).match({nama:  keyword, status: "publish"}).limit(20);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}

		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});

// List untuk datatable
router.get('/bangunan/list/', function(req, res, next) {
	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){
        	let keyword          = req.query.search;
		    let limit           = parseInt( req.query.limit );
			let offset = 0;
		    if( keyword !== null ){
		        offset = parseInt( req.query.offset );
		    }
		    let order           = req.query.order;
		    let order_column    = parseInt( req.query.order_column );

		    let status 			= req.query.status;

		    let total = await Tanah_bangunan.count();

		    let search = new RegExp(keyword, 'i');
			
			if( status === "trash" ){
				Tanah_bangunan.find({ 
					$and: [
			          { $or: [{status : "trash"}] },
			          { kode: search, nama: search }
			        ]} , function (err, result) {
			        if (err) throw err;
			        let data    =  result;

			        res.json( {
			            recordsTotal: total,
			            recordsFiltered: limit,
			            data: data
			        } );


			    }).limit(limit).skip(offset).sort({_id:-1});
			}else{
				Tanah_bangunan.find({ 
					$and: [
			          { $or: [{status : "publish"}, {status : "draft"}] },
			          { kode: search, nama: search }
			        ]} , function (err, result) {
			        if (err) throw err;
			        let data    =  result;

			        res.json( {
			            recordsTotal: total,
			            recordsFiltered: limit,
			            data: data
			        } );


			    }).limit(limit).skip(offset).sort({_id:-1});
			}
			
        }
    });

});

// get single record
router.get('/bangunan/get/:id', function(req, res, next) {
	let token 		= req.header('api_token');
    let id = req.params.id;

	Otentikasi(token, async function (result){
        if (result){

		 const tanah = await Tanah_bangunan.aggregate([
			   {
			     $lookup:
			       {
			         from: "kabupaten",
			         localField: "kabupaten_id",
			         foreignField: "kabupaten_id",
			         as: "kabupaten"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "kecamatan",
			         localField: "kecamatan_id",
			         foreignField: "kecamatan_id",
			         as: "kecamatan"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "desa",
			         localField: "desa_id",
			         foreignField: "desa_id",
			         as: "desa"
			       }
			  }
			]).match({_id: new ObjectId(id) }).limit(1);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}

		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});


router.post('/bangunan/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }, 
					{ name: 'foto_depan', maxSize: '1MB' },
					{ name: 'foto_belakang', maxSize: '1MB' },
					{ name: 'foto_kanan', maxSize: '1MB' },
					{ name: 'foto_kiri', maxSize: '1MB' },
					{ name: 'foto_atas', maxSize: '1MB' } 
				]);

			

			upload(req, res, function(err) {
				let foto_depan 		= ( req.files.foto_depan != undefined )?'upload/'+req.files.foto_depan[0].filename:null;
	        	let foto_belakang 	= ( req.files.foto_belakang != undefined )?'upload/'+req.files.foto_belakang[0].filename:null;
	        	let foto_kanan 		= ( req.files.foto_kanan != undefined )?'upload/'+req.files.foto_kanan[0].filename:null;
	        	let foto_kiri 		= ( req.files.foto_kiri != undefined )?'upload/'+req.files.foto_kiri[0].filename:null;
	        	let foto_atas 		= ( req.files.foto_atas != undefined )?'upload/'+req.files.foto_atas[0].filename:null;
	        	let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null;
	        	let file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null;

	        	let geolocation = null;
	        	if( file_geojson == null ){
	        		// jika koordinat dientri manual
		        	let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr } 
	        	}

		        if (req.fileValidationError) {

		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		// Simpan tanpa upload file
		        		let pre_data = {
		        			_id: new ObjectId,
		        			kode: req.body.kode,
		        			kabupaten_id: req.body.kabupaten_id,
		        			kecamatan_id: req.body.kecamatan_id,
		        			desa_id: req.body.desa_id,
		        			nomor_sertifikat: req.body.nomor_sertifikat,
		        			tanggal_sertifikat: req.body.tanggal_sertifikat,
		        			luas: req.body.luas,
		        			nama: req.body.nama,
		        			nomor_izin: req.body.nomor_izin,
		        			tanggal_izin: req.body.tanggal_izin,
		        			peruntukan: req.body.peruntukan,
							file_sertifikat: null,
							foto_depan: null,
							foto_belakang: null,
							foto_kanan: null,
							foto_kiri: null,
							foto_atas: null,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "draft"

		        		};


		        		Tanah_bangunan.create( pre_data ).then( msg => {
		        			res.json({status : 'success', data : pre_data._id});
		        		})

		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

		        	// Simpan dan upload file
		        	let pre_data = {
		        		_id: new ObjectId,
		        		kode: req.body.kode,
	        			kabupaten_id: req.body.kabupaten_id,
	        			kecamatan_id: req.body.kecamatan_id,
	        			desa_id: req.body.desa_id,
	        			nomor_sertifikat: req.body.nomor_sertifikat,
	        			tanggal_sertifikat: req.body.tanggal_sertifikat,
	        			luas: req.body.luas,
	        			nama: req.body.nama,
	        			nomor_izin: req.body.nomor_izin,
	        			tanggal_izin: req.body.tanggal_izin,
	        			peruntukan: req.body.peruntukan,
						file_sertifikat: file_sertifikat,
						foto_depan: foto_depan,
						foto_belakang: foto_belakang,
						foto_kanan: foto_kanan,
						foto_kiri: foto_kiri,
						foto_atas: foto_atas,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "draft"
	        		};

	        		Tanah_bangunan.create( pre_data ).then( msg => {
	        			console.log( pre_data )
	        			res.json({status : 'success', data : pre_data._id});
	        		})
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.put('/bangunan/edit/:id', function(req, res, next) {
    let token = req.header('api_token');
    let id = req.params.id;

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "application/geo+json": ".geojson",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }, 
					{ name: 'foto_depan', maxSize: '1MB' },
					{ name: 'foto_belakang', maxSize: '1MB' },
					{ name: 'foto_kanan', maxSize: '1MB' },
					{ name: 'foto_kiri', maxSize: '1MB' },
					{ name: 'foto_atas', maxSize: '1MB' } 
				]);

			

			upload(req, res, function(err) {
				
	        	let obj = {}
	        	if( req.files.file_geojson != undefined ){
	        		Object.assign(obj, {file_geojson: 'upload/'+req.files.file_geojson[0].filename})
	        	}

	        	if( req.files.file_sertifikat != undefined ){
	        		Object.assign(obj, {file_sertifikat: 'upload/'+req.files.file_sertifikat[0].filename})
	        	}

	        	if( req.files.foto_depan != undefined ){
	        		Object.assign(obj, {foto_depan: 'upload/'+req.files.foto_depan[0].filename})
	        	}

	        	if( req.files.foto_belakang != undefined ){
	        		Object.assign(obj, {foto_belakang: 'upload/'+req.files.foto_belakang[0].filename})
	        	}

	        	if( req.files.foto_kanan != undefined ){
	        		Object.assign(obj, {foto_kanan: 'upload/'+req.files.foto_kanan[0].filename})
	        	}

	        	if( req.files.foto_kiri != undefined ){
	        		Object.assign(obj, {foto_kiri: 'upload/'+req.files.foto_kiri[0].filename})
	        	}

	        	if( req.files.foto_atas != undefined ){
	        		Object.assign(obj, {foto_atas: 'upload/'+req.files.foto_atas[0].filename})
	        	}


		        if (req.fileValidationError) {

		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		// Simpan tanpa upload file
		        		let pre_data = {
		        			kabupaten_id: req.body.kabupaten_id,
		        			kecamatan_id: req.body.kecamatan_id,
		        			desa_id: req.body.desa_id,
		        			nomor_sertifikat: req.body.nomor_sertifikat,
		        			tanggal_sertifikat: req.body.tanggal_sertifikat,
		        			luas: req.body.luas,
		        			nama: req.body.nama,
		        			nomor_izin: req.body.nomor_izin,
		        			tanggal_izin: req.body.tanggal_izin,
		        			peruntukan: req.body.peruntukan,
							geolocation: geolocation

		        		};

		        		let input = Object.assign(obj, pre_data)

		        		Tanah_bangunan.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
			                if (err) throw err;

			                res.json({status : 'success', data : true});
			            });


		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

		        	// Simpan dan upload file
		        	let pre_data = {
	        			kabupaten_id: req.body.kabupaten_id,
	        			kecamatan_id: req.body.kecamatan_id,
	        			desa_id: req.body.desa_id,
	        			nomor_sertifikat: req.body.nomor_sertifikat,
	        			tanggal_sertifikat: req.body.tanggal_sertifikat,
	        			luas: req.body.luas,
	        			nama: req.body.nama,
	        			nomor_izin: req.body.nomor_izin,
	        			tanggal_izin: req.body.tanggal_izin,
	        			peruntukan: req.body.peruntukan
	        		};

	        		let input = Object.assign(obj, pre_data)

	        		Tanah_bangunan.findOneAndUpdate({_id : new ObjectId(id)}, {$set:input}, function (err, result1){
		                if (err) throw err;

		                res.json({status : 'success', data : true});
		            });
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.post('/bangunan/publish/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
        	let status = (req.body.status == "publish" )?"publish":"draft";

            Tanah_bangunan.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: status}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

// Delete
router.delete('/bangunan/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let status 	= req.query.status;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
        	Tanah_bangunan.find({ _id : new ObjectId(id)} , function (err, result) {
		        if (err) throw err;
		        let data    =  result;
		        if( result ){

		        	if( status == "permanent" ){
		        		/* Hapus Permanent */
		        		// Hapus file
						fs.unlink("public/"+result[0].file_sertifikat, function (err) {}); 
						fs.unlink("public/"+result[0].file_geojson, function (err) {}); 
						fs.unlink("public/"+result[0].foto_depan, function (err) {}); 
						fs.unlink("public/"+result[0].foto_belakang, function (err) {}); 
						fs.unlink("public/"+result[0].foto_kanan, function (err) {}); 
						fs.unlink("public/"+result[0].foto_kiri, function (err) {}); 
						fs.unlink("public/"+result[0].foto_atas, function (err) {}); 

						Tanah_bangunan.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
			                if (err) throw err;
			                res.json({status: 'success', data: true});
			            });
		        	}else{
		        		/* Masukkan Tempat Sampah  */
		        		Tanah_bangunan.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
							if (err) throw err;

							res.json({status : 'success', data : true});
						});
		        	}
		        }
		        
		    }).limit(1);

            // Tanah_bangunan.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
            //     if (err) throw err;

            //     res.json({status : 'success', data : true});
            // });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

/* -------- Akhir Tanah Bangunan Pemerintah -------- */


/* -------- Tanah Kas Gampong -------- */

// Ambil nomor terakhir urut untuk kode pertanahan
router.get('/tanah_kas_gampong/nomor_urut/', function(req, res, next) {

    Tanah_kas_gampong.countDocuments({}, function (err, result) {
        if (err) throw err;

        res.json( { data: {kode_pertanahan: kode.tanah_kas_gampong, urut:result+1} } );
    });
});

// List untuk datatable
router.get('/tanah_kas_gampong/list/', function(req, res, next) {
	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){
        	let keyword          = req.query.search;
		    let limit           = parseInt( req.query.limit );
			let offset = 0;
		    if( keyword !== null ){
		        offset = parseInt( req.query.offset );
		    }
		    let order           = req.query.order;
		    let order_column    = parseInt( req.query.order_column );

		    let status 			= null;
		    if( req.query.status ){
		    	status = req.query.status;
		    }else{
		    	status = "publish";
		    }

		    let total = await Tanah_kas_gampong.count();

		    let search = new RegExp(keyword, 'i');
			Tanah_kas_gampong.find({ kode: search, status: status } , function (err, result) {
		        if (err) throw err;
		        let data    =  result;

		        res.json( {
		            recordsTotal: total,
		            recordsFiltered: limit,
		            data: data
		        } );
		    }).limit(limit).skip(offset).sort({_id:-1});
        }
    });


});


// get single record
router.get('/tanah_kas_gampong/get/:id', function(req, res, next) {
	let token 		= req.header('api_token');
    let id = req.params.id;

	Otentikasi(token, async function (result){
        if (result){

		 const tanah = await Tanah_kas_gampong.aggregate([
			   {
			     $lookup:
			       {
			         from: "kabupaten",
			         localField: "kabupaten_id",
			         foreignField: "kabupaten_id",
			         as: "kabupaten"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "kecamatan",
			         localField: "kecamatan_id",
			         foreignField: "kecamatan_id",
			         as: "kecamatan"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "desa",
			         localField: "desa_id",
			         foreignField: "desa_id",
			         as: "desa"
			       }
			  }
			]).match({_id: new ObjectId(id) }).limit(1);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}

		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});


router.post('/tanah_kas_gampong/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }, 
					{ name: 'foto_depan', maxSize: '1MB' },
					{ name: 'foto_belakang', maxSize: '1MB' },
					{ name: 'foto_kanan', maxSize: '1MB' },
					{ name: 'foto_kiri', maxSize: '1MB' },
					{ name: 'foto_atas', maxSize: '1MB' } 
				]);

			

			upload(req, res, function(err) {
				let foto_depan 		= ( req.files.foto_depan != undefined )?'upload/'+req.files.foto_depan[0].filename:null;
	        	let foto_belakang 	= ( req.files.foto_belakang != undefined )?'upload/'+req.files.foto_belakang[0].filename:null;
	        	let foto_kanan 		= ( req.files.foto_kanan != undefined )?'upload/'+req.files.foto_kanan[0].filename:null;
	        	let foto_kiri 		= ( req.files.foto_kiri != undefined )?'upload/'+req.files.foto_kiri[0].filename:null;
	        	let foto_atas 		= ( req.files.foto_atas != undefined )?'upload/'+req.files.foto_atas[0].filename:null;
	        	let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null;
	        	let file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null;

	        	let geolocation = null;
	        	if( file_geojson == null ){
	        		// jika koordinat dientri manual
		        	let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr } 
	        	}

		        if (req.fileValidationError) {

		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		// Simpan tanpa upload file
		        		let pre_data = {
		        			_id: new ObjectId,
		        			kode: req.body.kode,
		        			kabupaten_id: req.body.kabupaten_id,
		        			kecamatan_id: req.body.kecamatan_id,
		        			desa_id: req.body.desa_id,
		        			nomor_sertifikat: req.body.nomor_sertifikat,
		        			tanggal_sertifikat: req.body.tanggal_sertifikat,
		        			luas: req.body.luas,
		        			nama: req.body.nama,
		        			nomor_izin: req.body.nomor_izin,
		        			tanggal_izin: req.body.tanggal_izin,
		        			asal_usul: req.body.asal_usul,
		        			peruntukan: req.body.peruntukan,
							file_sertifikat: null,
							foto_depan: null,
							foto_belakang: null,
							foto_kanan: null,
							foto_kiri: null,
							foto_atas: null,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "publish"

		        		};


		        		Tanah_kas_gampong.create( pre_data ).then( msg => {
		        			res.json({status : 'success', data : pre_data._id});
		        		})

		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

		        	// Simpan dan upload file
		        	let pre_data = {
		        		_id: new ObjectId,
		        		kode: req.body.kode,
	        			kabupaten_id: req.body.kabupaten_id,
	        			kecamatan_id: req.body.kecamatan_id,
	        			desa_id: req.body.desa_id,
	        			nomor_sertifikat: req.body.nomor_sertifikat,
	        			tanggal_sertifikat: req.body.tanggal_sertifikat,
	        			luas: req.body.luas,
	        			nama: req.body.nama,
	        			nomor_izin: req.body.nomor_izin,
	        			tanggal_izin: req.body.tanggal_izin,
	        			peruntukan: req.body.peruntukan,
	        			asal_usul: req.body.asal_usul,
						file_sertifikat: file_sertifikat,
						foto_depan: foto_depan,
						foto_belakang: foto_belakang,
						foto_kanan: foto_kanan,
						foto_kiri: foto_kiri,
						foto_atas: foto_atas,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "publish"
	        		};

	        		Tanah_kas_gampong.create( pre_data ).then( msg => {
	        			console.log( pre_data )
	        			res.json({status : 'success', data : pre_data._id});
	        		})
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


// Delete
router.delete('/tanah_kas_gampong/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Tanah_kas_gampong.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : null});
        }
    });
});

/* -------- Akhir Tanah Kas Gampong -------- */





/* -------- Tanah Persil Masyarakat -------- */

// Ambil nomor terakhir urut untuk kode pertanahan
router.get('/tanah_persil_masyarakat/nomor_urut/', function(req, res, next) {

    Tanah_persil_masyarakat.countDocuments({}, function (err, result) {
        if (err) throw err;

        res.json( { data: {kode_pertanahan: kode.tanah_persil_masyarakat, urut:result+1} } );
    });
});

// List untuk datatable
router.get('/tanah_persil_masyarakat/list/', function(req, res, next) {
	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){
        	let keyword          = req.query.search;
		    let limit           = parseInt( req.query.limit );
			let offset = 0;
		    if( keyword !== null ){
		        offset = parseInt( req.query.offset );
		    }
		    let order           = req.query.order;
		    let order_column    = parseInt( req.query.order_column );

		    let status 			= null;
		    if( req.query.status ){
		    	status = req.query.status;
		    }else{
		    	status = "publish";
		    }

		    let total = await Tanah_persil_masyarakat.count();

		    let search = new RegExp(keyword, 'i');
			Tanah_persil_masyarakat.find({ kode: search, status: status } , function (err, result) {
		        if (err) throw err;
		        let data    =  result;

		        res.json( {
		            recordsTotal: total,
		            recordsFiltered: limit,
		            data: data
		        } );
		    }).limit(limit).skip(offset).sort({_id:-1});
        }
    });


});


// get single record
router.get('/tanah_persil_masyarakat/get/:id', function(req, res, next) {
	let token 		= req.header('api_token');
    let id = req.params.id;

	Otentikasi(token, async function (result){
        if (result){

		 const tanah = await Tanah_persil_masyarakat.aggregate([
			   {
			     $lookup:
			       {
			         from: "kabupaten",
			         localField: "kabupaten_id",
			         foreignField: "kabupaten_id",
			         as: "kabupaten"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "kecamatan",
			         localField: "kecamatan_id",
			         foreignField: "kecamatan_id",
			         as: "kecamatan"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "desa",
			         localField: "desa_id",
			         foreignField: "desa_id",
			         as: "desa"
			       }
			  }
			]).match({_id: new ObjectId(id) }).limit(1);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}

		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});



router.post('/tanah_persil_masyarakat/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }, 
					{ name: 'foto_depan', maxSize: '1MB' },
					{ name: 'foto_belakang', maxSize: '1MB' },
					{ name: 'foto_kanan', maxSize: '1MB' },
					{ name: 'foto_kiri', maxSize: '1MB' },
					{ name: 'foto_atas', maxSize: '1MB' } 
				]);

			

			upload(req, res, function(err) {
				let foto_depan 		= ( req.files.foto_depan != undefined )?'upload/'+req.files.foto_depan[0].filename:null;
	        	let foto_belakang 	= ( req.files.foto_belakang != undefined )?'upload/'+req.files.foto_belakang[0].filename:null;
	        	let foto_kanan 		= ( req.files.foto_kanan != undefined )?'upload/'+req.files.foto_kanan[0].filename:null;
	        	let foto_kiri 		= ( req.files.foto_kiri != undefined )?'upload/'+req.files.foto_kiri[0].filename:null;
	        	let foto_atas 		= ( req.files.foto_atas != undefined )?'upload/'+req.files.foto_atas[0].filename:null;
	        	let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null;
	        	let file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null;

	        	let geolocation = null;
	        	if( file_geojson == null ){
	        		// jika koordinat dientri manual
		        	let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr } 
	        	}

		        if (req.fileValidationError) {

		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		// Simpan tanpa upload file
		        		let pre_data = {
		        			_id: new ObjectId,
		        			kode: req.body.kode,
		        			kabupaten_id: req.body.kabupaten_id,
		        			kecamatan_id: req.body.kecamatan_id,
		        			desa_id: req.body.desa_id,
		        			nomor_sertifikat: req.body.nomor_sertifikat,
		        			tanggal_sertifikat: req.body.tanggal_sertifikat,
		        			luas: req.body.luas,
		        			nama: req.body.nama,
		        			nomor_izin: req.body.nomor_izin,
		        			tanggal_izin: req.body.tanggal_izin,
		        			asal_usul: req.body.asal_usul,
		        			peruntukan: req.body.peruntukan,
							file_sertifikat: null,
							foto_depan: null,
							foto_belakang: null,
							foto_kanan: null,
							foto_kiri: null,
							foto_atas: null,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "publish"

		        		};


		        		Tanah_persil_masyarakat.create( pre_data ).then( msg => {
		        			res.json({status : 'success', data : pre_data._id});
		        		})

		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

		        	// Simpan dan upload file
		        	let pre_data = {
		        		_id: new ObjectId,
		        		kode: req.body.kode,
	        			kabupaten_id: req.body.kabupaten_id,
	        			kecamatan_id: req.body.kecamatan_id,
	        			desa_id: req.body.desa_id,
	        			nomor_sertifikat: req.body.nomor_sertifikat,
	        			tanggal_sertifikat: req.body.tanggal_sertifikat,
	        			luas: req.body.luas,
	        			nama: req.body.nama,
	        			nomor_izin: req.body.nomor_izin,
	        			tanggal_izin: req.body.tanggal_izin,
	        			peruntukan: req.body.peruntukan,
	        			asal_usul: req.body.asal_usul,
						file_sertifikat: file_sertifikat,
						foto_depan: foto_depan,
						foto_belakang: foto_belakang,
						foto_kanan: foto_kanan,
						foto_kiri: foto_kiri,
						foto_atas: foto_atas,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "publish"
	        		};

	        		Tanah_persil_masyarakat.create( pre_data ).then( msg => {
	        			res.json({status : 'success', data : pre_data._id});
	        		})
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});



// Delete
router.delete('/tanah_persil_masyarakat/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Tanah_persil_masyarakat.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

/* -------- Akhir Tanah Persil Masyarakat -------- */


/* -------- Tanah Wakaf -------- */

// Ambil nomor terakhir urut untuk kode pertanahan
router.get('/tanah_wakaf/nomor_urut/', function(req, res, next) {

    Tanah_wakaf.countDocuments({}, function (err, result) {
        if (err) throw err;

        res.json( { data: {kode_pertanahan: kode.tanah_wakaf, urut:result+1} } );
    });
});

// List untuk datatable
router.get('/tanah_wakaf/list/', function(req, res, next) {
	const token 		= req.header('api_token');

	Otentikasi(token, async function (result){
        if (result){
        	let keyword          = req.query.search;
		    let limit           = parseInt( req.query.limit );
			let offset = 0;
		    if( keyword !== null ){
		        offset = parseInt( req.query.offset );
		    }
		    let order           = req.query.order;
		    let order_column    = parseInt( req.query.order_column );

		    let status 			= null;
		    if( req.query.status ){
		    	status = req.query.status;
		    }else{
		    	status = "publish";
		    }

		    let total = await Tanah_wakaf.count();

		    let search = new RegExp(keyword, 'i');
			Tanah_wakaf.find({ kode: search, status: status } , function (err, result) {
		        if (err) throw err;
		        let data    =  result;

		        res.json( {
		            recordsTotal: total,
		            recordsFiltered: limit,
		            data: data
		        } );
		    }).limit(limit).skip(offset).sort({_id:-1});
        }
    });


});


// get single record
router.get('/tanah_wakaf/get/:id', function(req, res, next) {
	let token 		= req.header('api_token');
    let id = req.params.id;

	Otentikasi(token, async function (result){
        if (result){

		 const tanah = await Tanah_wakaf.aggregate([
			   {
			     $lookup:
			       {
			         from: "kabupaten",
			         localField: "kabupaten_id",
			         foreignField: "kabupaten_id",
			         as: "kabupaten"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "kecamatan",
			         localField: "kecamatan_id",
			         foreignField: "kecamatan_id",
			         as: "kecamatan"
			       }
			  },
			  {
			     $lookup:
			       {
			         from: "desa",
			         localField: "desa_id",
			         foreignField: "desa_id",
			         as: "desa"
			       }
			  }
			]).match({_id: new ObjectId(id) }).limit(1);

			if( tanah.length > 0 ){
				res.json({status: 'success', data: tanah[0] });
			}else{
				res.json({status: 'fail', data: "Data tidak ditemukan" });
			}

		

        }else{
        	res.json({status : 'denied', data : "Token tidak valid"});
        }
    });

});



router.post('/tanah_wakaf/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
        	// Upload file
        	let storage = multer.diskStorage({
				destination: function (req, file, cb) {
				  cb(null, 'public/upload')
				},
				filename: function (req, file, cb) {
				  let fileObj = {
				    "application/pdf": ".pdf",
				    "application/json": ".json",
				    "image/png": ".png",
				    "image/jpeg": ".jpeg",
				    "image/jpg": ".jpg"
				  };

				  if (fileObj[file.mimetype] == undefined) {
				    cb( "Jenis file tidak valid" );
				  } else {
				    cb(null, Date.now() + fileObj[file.mimetype])
				  }
				} 
			});

			let upload 		= multer({ storage: storage }).fields(	
				[
					{ name: 'file_sertifikat', maxCount: 1, maxSize: '2MB' }, 
					{ name: 'file_geojson', maxCount: 1, maxSize: '1MB' }, 
					{ name: 'foto_depan', maxSize: '1MB' },
					{ name: 'foto_belakang', maxSize: '1MB' },
					{ name: 'foto_kanan', maxSize: '1MB' },
					{ name: 'foto_kiri', maxSize: '1MB' },
					{ name: 'foto_atas', maxSize: '1MB' } 
				]);

			

			upload(req, res, function(err) {
				let foto_depan 		= ( req.files.foto_depan != undefined )?'upload/'+req.files.foto_depan[0].filename:null;
	        	let foto_belakang 	= ( req.files.foto_belakang != undefined )?'upload/'+req.files.foto_belakang[0].filename:null;
	        	let foto_kanan 		= ( req.files.foto_kanan != undefined )?'upload/'+req.files.foto_kanan[0].filename:null;
	        	let foto_kiri 		= ( req.files.foto_kiri != undefined )?'upload/'+req.files.foto_kiri[0].filename:null;
	        	let foto_atas 		= ( req.files.foto_atas != undefined )?'upload/'+req.files.foto_atas[0].filename:null;
	        	let file_sertifikat = ( req.files.file_sertifikat != undefined )?'upload/'+req.files.file_sertifikat[0].filename:null;
	        	let file_geojson 	= ( req.files.file_geojson != undefined )?'upload/'+req.files.file_geojson[0].filename:null;

	        	let geolocation = null;
	        	if( file_geojson == null ){
	        		// jika koordinat dientri manual
		        	let req_koordinat = req.body.koordinat;
					let koordinat_arr = []
					for( let x in req_koordinat ){
						let k = JSON.parse( req_koordinat[x] );
						let filter = [k.latitude = parseFloat( k.latitude ), k.longitude = parseFloat( k.longitude )];
						koordinat_arr.push( filter )
					}

					geolocation =  { type: "MultiPoint", coordinates: koordinat_arr } 
	        	}

		        if (req.fileValidationError) {

		        	res.json({status : 'fail', data : req.fileValidationError});

		        }else if (!req.files) {
		        	if( !err ){

		        		// Simpan tanpa upload file
		        		let pre_data = {
		        			_id: new ObjectId,
		        			kode: req.body.kode,
		        			kabupaten_id: req.body.kabupaten_id,
		        			kecamatan_id: req.body.kecamatan_id,
		        			desa_id: req.body.desa_id,
		        			nomor_sertifikat: req.body.nomor_sertifikat,
		        			tanggal_sertifikat: req.body.tanggal_sertifikat,
		        			luas: req.body.luas,
		        			nama: req.body.nama,
		        			nomor_izin: req.body.nomor_izin,
		        			tanggal_izin: req.body.tanggal_izin,
		        			asal_usul: req.body.asal_usul,
		        			peruntukan: req.body.peruntukan,
							file_sertifikat: null,
							foto_depan: null,
							foto_belakang: null,
							foto_kanan: null,
							foto_kiri: null,
							foto_atas: null,
							geolocation: geolocation,
							file_geojson: file_geojson,
							status: "publish"

		        		};


		        		Tanah_wakaf.create( pre_data ).then( msg => {
		        			res.json({status : 'success', data : pre_data._id});
		        		})

		        	}else{
		        		res.json({status : 'fail', data : err});
		        	}
		        }
		        else if (err instanceof multer.MulterError) {
		            res.json({status : 'fail', data : err});
		        }else{

		        	// Simpan dan upload file
		        	let pre_data = {
		        		_id: new ObjectId,
		        		kode: req.body.kode,
	        			kabupaten_id: req.body.kabupaten_id,
	        			kecamatan_id: req.body.kecamatan_id,
	        			desa_id: req.body.desa_id,
	        			nomor_sertifikat: req.body.nomor_sertifikat,
	        			tanggal_sertifikat: req.body.tanggal_sertifikat,
	        			luas: req.body.luas,
	        			nama: req.body.nama,
	        			nomor_izin: req.body.nomor_izin,
	        			tanggal_izin: req.body.tanggal_izin,
	        			peruntukan: req.body.peruntukan,
	        			asal_usul: req.body.asal_usul,
						file_sertifikat: file_sertifikat,
						foto_depan: foto_depan,
						foto_belakang: foto_belakang,
						foto_kanan: foto_kanan,
						foto_kiri: foto_kiri,
						foto_atas: foto_atas,
						geolocation: geolocation,
						file_geojson: file_geojson,
						status: "publish"
	        		};

	        		Tanah_wakaf.create( pre_data ).then( msg => {
	        			res.json({status : 'success', data : pre_data._id});
	        		})
		        }
		        
		    });

        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});



// Delete
router.delete('/tanah_wakaf/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Tanah_wakaf.findOneAndUpdate({_id : new ObjectId(id)}, {$set:{status: "trash"}}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

/* -------- Akhir Tanah Wakaf -------- */



module.exports = router;