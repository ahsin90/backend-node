var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var mongo = require('mongodb').MongoClient;
var ObjectId = require('mongodb').ObjectID;

let Users = require('../models/users.model');
let Otentikasi = require('../helpers/otentikasi');
let Provinsi = require('../models/provinsi.model');
let Kabupaten = require('../models/kabupaten.model');
let Kecamatan = require('../models/kecamatan.model');
let Desa = require('../models/desa.model');
let Master_Wilayah = require('../models/master_wilayah.model');

router.get('/provinsi/list/', async function(req, res, next) {
	const token 		= req.header('api_token');
    let keyword         = req.query.search;
    let limit           = parseInt( req.query.limit );

    let offset = 0;
    if( keyword !== null ){
        offset = parseInt( req.query.offset );
    }

    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Provinsi.count();

    let search = new RegExp(keyword, 'i');
	Master_Wilayah.find({ nama: search, tingkat: 1} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            recordsTotal: total,
            recordsFiltered: limit,
            data: data
        } );
    }).limit(limit).skip(offset).sort({nama:1});

});

router.post('/provinsi/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOne({provinsi_id : req.body.provinsi_id}, function (err, doc){

                if (doc != null){
                    res.json({status : 'failed', data : 'Kode provinsi sudah ada'});
                }
                else{
                    let prov = new Master_Wilayah(req.body);
                    prov.save().then(prov =>{
                        res.json({status : 'success', data : true});
                    }).catch(err =>{
                        throw err;
                    });
                }
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/provinsi/get/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOne({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
         else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/provinsi/edit/:id', function (req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            
            Master_Wilayah.findOneAndUpdate({_id : new ObjectId(id)}, {$set:req.body}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.delete('/provinsi/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/provinsi/get/', function(req, res, next) {
	Master_Wilayah.find({tingkat:1} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
						status: "success",
            data: data
        } );
    }).sort({nama:1});

});

router.get('/kabupaten/list/', async function(req, res, next) {
    const token         = req.header('api_token');
    let keyword          = req.query.search;
    let limit           = parseInt( req.query.limit );
    let offset           = parseInt( req.query.offset );
    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Master_Wilayah.count({tingkat:2});

    let search = new RegExp(keyword, 'i');
    Master_Wilayah.find({ nama: search, tingkat: 2} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            recordsTotal: total,
            recordsFiltered: limit,
            data: data
        } );
    }).limit(limit).skip(offset).sort({_id:1});

});

router.get('/kabupaten/list_public/:id', async function(req, res, next) {
    const prov_id     = req.params.id;
    Master_Wilayah.find({ tingkat: 2} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {status : 'success', data: data} );
    }).sort({nama:1});

});

router.post('/kabupaten/add', function(req, res, next) {
    let token = req.header('api_token');

    // console.log(Math.round(Math.random() * 10000))

    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOne({kode : req.body.kode}, function (err, doc){

                if (doc != null){
                    res.json({status : 'failed', data : 'Kode kabupaten sudah ada'});
                }
                else{
                    req.body._id        = new ObjectId();
                    req.body.id         = Math.round(Math.random() * 10000);
                    req.body.tingkat    = 2;
                    req.body.provinsi   = 1;
                    req.body.kabupaten  = req.body.id;
                    req.body.kecamatan  = null;
                    req.body.gampong    = null;

                    let prov = new Master_Wilayah(req.body);
                    prov.save().then(prov =>{
                        res.json({status : 'success', data : true});
                    }).catch(err =>{
                        throw err;
                    });
                }
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/kabupaten/get/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOne({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
         else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/kabupaten/edit/:id', function (req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){

            Master_Wilayah.findOneAndUpdate({_id : new ObjectId(id)}, {$set:req.body}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.delete('/kabupaten/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/kabupaten/get_by_provinsi/:provinsi_id', function(req, res, next) {
	let id = req.params.provinsi_id;
	Master_Wilayah.find({provinsi: 1, tingkat:2} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
						status: "success",
            data: data
        } );
    }).sort({nama:1});

});


router.get('/kecamatan/list/', async function(req, res, next) {
    const token         = req.header('api_token');
    let keyword         = req.query.search;
    let limit           = parseInt( req.query.limit );
    let offset          = parseInt( req.query.offset );
    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Master_Wilayah.count();

    let search = new RegExp(keyword, 'i');
    Master_Wilayah.find({nama: search, tingkat: 3} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            recordsTotal: total,
            recordsFiltered: limit,
            data: data
        } );
    }).limit(limit).skip(offset).sort({nama:1});
});

router.get('/kecamatan/list_public_by_kabupaten_id/:id', async function(req, res, next) {
    const token         = req.header('api_token');
    let ids = req.params.id;

    Master_Wilayah.find({id: ids} , function (err, result) {
        if (err) throw err;

        res.json( {status: 'success', data: result});
    }).sort({nama:1});
});

router.post('/kecamatan/add', function(req, res, next) {
    let token = req.header('api_token');

    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOne({kode : req.body.kode}, function (err, doc){

                if (doc != null){
                    res.json({status : 'failed', data : 'Kode kecamatan sudah ada'});
                }
                else{
                    req.body._id        = new ObjectId();
                    req.body.id         = Math.round(Math.random() * 10000);
                    req.body.tingkat    = 3;
                    req.body.provinsi   = 1;
                    req.body.kecamatan  = req.body.id;
                    req.body.gampong    = null;
                    let prov = new Master_Wilayah(req.body);
                    prov.save().then(prov =>{
                        res.json({status : 'success', data : true});
                    }).catch(err =>{
                        throw err;
                    });
                }
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/kecamatan/get/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOne({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
         else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/kecamatan/get_by_id/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Kecamatan.findOne({kecamatan_id: id}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: result});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});


router.put('/kecamatan/edit/:id', function (req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOneAndUpdate({_id : new ObjectId(id)}, {$set:req.body}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.delete('/kecamatan/delete/:id', function(req, res, next) {
    let id = req.params.id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Master_Wilayah.findOneAndDelete({_id: new ObjectId(id)}, function (err, result) {
                if (err) throw err;
                res.json({status: 'success', data: true});
            });
        }
        else
        {
            res.json({status : 'denied', data : "Token tidak valid"});
        }
    });
});

router.get('/kecamatan/get_by_kabupaten/:kabupaten_id', function(req, res, next) {
	let id = req.params.kabupaten_id;
    
	Master_Wilayah.find({kabupaten: id, tingkat: 3} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
						status: "success",
            data: data
        } );
    }).sort({kecamatan:1});

});


router.get('/desa/list/', async function(req, res, next) {
    const token         = req.header('api_token');
    let keyword         = req.query.search;
    let limit           = parseInt( req.query.limit );
    let offset          = parseInt( req.query.offset );
    let order           = req.query.order;
    let order_column    = parseInt( req.query.order_column );

    let total = await Master_Wilayah.count({tingkat:4});

    let search = new RegExp(keyword, 'i');

    Master_Wilayah.find({nama: search, tingkat: 4} , function (err, result) {
        if (err) throw err;
        let data    =  result;

        res.json( {
            recordsTotal: total,
            recordsFiltered: limit,
            data: data
        } );
    }).limit(limit).skip(offset).sort({nama:1});
});


router.get('/desa/get_by_kecamatan/:kecamatan_id', function(req, res, next) {
    let id = req.params.kecamatan_id;
    
    Master_Wilayah.find({kecamatan: id, tingkat:4} , function (err, result) {
        if (err) throw err;
        let data    =  result;
        res.json( {
                        status: "success",
            data: data
        } );
    }).sort({nama:1});

});

module.exports = router;
