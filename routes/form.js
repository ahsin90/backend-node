var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;

let Forms = require('../models/forms.model');
let FormData = require('../models/formdata.model');

let Otentikasi = require('../helpers/otentikasi');

router.get('/get_submodule_form/:submodule_id', function(req, res, next) {
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Forms.findOne({submodule_id : new ObjectId(submodule_id)}, function (err, result1){
                if (err) throw err;

                res.json({status : 'success', data : result1});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/get_submodule_items_datatable/:submodule_id', function (req, res, next) {
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Forms.findOne({submodule_id : new ObjectId(submodule_id)}, function (err, result1) {
                if (result1 != null){
                    console.log(result1);
                    let coloms = result1._doc.inputs.map(function(item) {
                        if (item.datatable.includes("show")){
                            return item.field_name;
                        }
                    });
                    FormData.find({submodule_id : new ObjectId(submodule_id)}, function (err, result2){
                        if (err) throw err;
                        let retval = [];
                        result2.map(function (item){
                            let obj = {};
                            obj['id'] = item._id;
                            coloms.map(function(col){
                                if (item._doc.hasOwnProperty(col))
                                {
                                    obj[col] = item._doc[col];

                                }
                            });
                            retval.push(obj);
                        });
                        res.json( {
                            recordsTotal: retval.length,
                            recordsFiltered: 10,
                            data: retval
                        } );
                    });
                }
                else{
                    res.json({status : 'error', data : 'No data'});
                }
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.get('/get_submodule_item/:submodule_id/:item_id', function(req, res, next){
    let item_id = req.params.item_id;
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result) {
        if (result) {
            FormData.findOne({_id: new ObjectId(item_id)}, function (err, result1) {
                res.json({status : 'success', data : result1});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.post('/add_submodule_item/:submodule_id', function(req, res, next) {
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result) {
        if (result) {
            req.body._id = new ObjectId();
            req.body.submodule_id = new ObjectId(submodule_id);
            FormData.create(req.body).then(fd =>{
                res.json({status : 'success', data : true});
            }).catch(err =>{
                throw err;
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.put('/edit_submodule_item/:submodule_id/:item_id', function(req, res, next) {
    let item_id = req.params.item_id;
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result) {
        if (result) {
            FormData.findOneAndUpdate({_id : new ObjectId(item_id)}, req.body, function (err, result2) {
                if (err) throw err;
                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.delete('/delete_submodule_item/:submodule_id/:item_id', function(req, res, next) {
    let item_id = req.params.item_id;
    let submodule_id = req.params.submodule_id;
    let token = req.header('api_token');
    Otentikasi(token, function (result) {
        if (result) {
            FormData.findOneAndDelete({_id : new ObjectId(item_id)},function (err, result2) {
                if (err) throw err;
                res.json({status : 'success', data : true});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});


module.exports = router;