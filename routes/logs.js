var express = require('express');
var router = express.Router();
var ObjectId = require('mongodb').ObjectID;
let Otentikasi = require('../helpers/otentikasi');
let Logs = require('../models/logs.model');

router.get('/get_logs', function (req, res, next){
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result){
            Logs.find().toArray(function (err, result){
                if (err) throw err;
                res.json({status : 'success', data : result});
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

router.post('/add', function (req, res, next){
    let token = req.header('api_token');
    Otentikasi(token, function (result){
        if (result.result){
            req.body.user_id = result.user_id;
            req.body.ip_address = req.header('x-forwarded-for') || req.connection.remoteAddress;
            req.body.created_at = new Date();
            req.body._id = new ObjectId();
            var log = new Logs(req.body);
            log.save().then(log => {
                res.json({status : 'success', data : true});
            }).catch(err =>{
                throw err;
            });
        }
        else{
            res.json({status : 'denied', data : null});
        }
    });
});

module.exports = router;