var express = require('express');
var path = require('path');
var cors = require('cors'); //disable on production
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var helmet = require('helmet');
var sanitize = require('mongo-sanitize');
const mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var aclRouter = require('./routes/acl');
var logRouter = require('./routes/logs');
var wilayah = require('./routes/wilayah');
var pertanahan = require('./routes/pertanahan');
var organisasi = require('./routes/organisasi');
var kawasan_hutan = require('./routes/kawasan_hutan');
var hgu = require('./routes/hak_guna_usaha');
var batasWilayah = require('./routes/batas_wilayah');
var iup = require('./routes/izin_usaha_pertambangan');
var batasWilayah = require('./routes/batas_wilayah');
var lokasiTransmigrasi = require('./routes/lokasi_transmigrasi');
var statistik = require('./routes/statistik');
var tanoh_gampong = require('./routes/tanoh_gampong');

var app = express({ env: process.env.NODE_ENV || 'production' });

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://'+process.env.DB_USER+':'+process.env.DB_PASSWORD+'@'+process.env.DB_HOST+':27017/'+process.env.DB,
    {
        useNewUrlParser: true ,
        useUnifiedTopology: true,
        auth:{authdb:"simtanah"}
    }).then(
    () => {console.log('Database is connected') },
    err => { console.log('Can not connect to the database '+ err)}
);


app.use(helmet());
app.use(cors()); // disable on production
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.disable('x-powered-by');


//disini middleware untuk filter parameter
var mySanitizer = function (req, res, next) {
    var bodyTemp = {};
    var headerTemp = {};

    for (var key in req.body) {
        bodyTemp[key] = sanitize(req.body[key]);
    }

    for (var key in req.headers) {
        headerTemp[key] = sanitize(req.headers[key]);
    }

    req.body = bodyTemp;
    req.headers = headerTemp;
    next();
};

app.use(mySanitizer); //untuk membersihkan query mongodb dalam parameter body, atau header
app.use('/', indexRouter);
app.use('/acl', aclRouter);
app.use('/logs', logRouter);
app.use('/wilayah', wilayah);
app.use('/pertanahan', pertanahan);
app.use('/batas_wilayah', batasWilayah);
app.use('/organisasi', organisasi);
app.use('/kawasan_hutan', kawasan_hutan);
app.use('/hak_guna_usaha', hgu);
app.use('/izin_usaha_pertambangan', iup);
app.use('/lokasi_transmigrasi', lokasiTransmigrasi);
app.use('/statistik', statistik);
app.use('/tanoh_gampong', tanoh_gampong);

module.exports = app;

